FROM ubuntu:18.04

RUN apt-get update && apt-get install -y \
    python \
    build-essential \
    mysql-client \
    libmysqlclient-dev \
    python-pip \
    python-dev

ADD requirements.txt /tmp/

RUN pip install --requirement /tmp/requirements.txt

ADD manage.py manage.py
ADD docker/docker-entrypoint.sh docker-entrypoint.sh
RUN chmod +x docker-entrypoint.sh
ADD docker/wait-for-it.sh wait-for-it.sh
RUN chmod +x  wait-for-it.sh

EXPOSE 8000

CMD ["./wait-for-it.sh", "mysql:3306", "--timeout=60", "--strict", "--", "./docker-entrypoint.sh"]