"""Fabric tasks for deployment of BCRData web app.
"""
import os

from fabric.api import cd
from fabric.api import env
from fabric.api import get
from fabric.api import local
from fabric.api import lcd
from fabric.api import run
from fabric.api import task
from fabric.api import prefix
from fabric.contrib.project import rsync_project


env.user = 'bcrandonneur'
env.hosts = ['www.randonneurs.bc.ca']
APP_NAME = 'bcrdata1_11'
HOME_DIR = '/home/{0}'.format(env.user)
APP_DIR = '/home/{0}/webapps/{1}'.format(env.user, APP_NAME)
PY_PATH = '{0}'.format(APP_DIR)

@task
def lint():
    """pylint the app"""
    local('pylint --load-plugins pylint_django bcrdatabase')

@task
def deploy():
    """Deploy app to webfaction
    """
    clean()
    #lint()
    #test()
    #    backup_database()
    deploy_code()
    install_deps()
    collect_static()
    perform_migration()
    fix_permissions()
    restart_apache()
    tag_git()
    

@task
def clean():
    """Clean up compiled files.
    """
    local(r'find . -name "*.pyc" -exec rm {} \;')

@task(default=True)
def runserver():
    """Runs the server
    """
    local(r'python manage.py runserver --settings=bcrdatabase.settings.dev')

@task
def test():
    """test
    """
    local(r'python manage.py test --settings=bcrdatabase.settings.test')

@task
def deploy_code():
    """rsync project code to webfaction
    """
    exclusions = (
        'dev_settings.py '
        'fabfile.py '
        '.DS_Store '
        '.git* '
        '*.db '
        '*.pyc '
        '*~ '
        'media '
        'ENV '
        'django17 '
        .split())
    rsync_project(remote_dir=APP_DIR, exclude=exclusions)

@task
def install_deps():
    """Install python dependencies
    """
    with cd(APP_DIR):
        with prefix('source {0}/virtualenvs/bcrdata1_11/bin/activate'.format(HOME_DIR)):
            run('pip install -r bcrdata/requirements.txt --upgrade')

@task
def collect_static():
    """Collect static files on webfaction
    """
    with cd(APP_DIR):
        with prefix('source {0}/virtualenvs/bcrdata1_11/bin/activate'.format(HOME_DIR)):
            run(r'python2.7 bcrdata/manage.py collectstatic --noinput --settings=bcrdatabase.settings.production')

@task
def backup_database():
    with cd(HOME_DIR):
        run('bin/backup-database.sh')

@task
def perform_migration():
    """Perform database migration
    """
    with cd(APP_DIR):
        with prefix('source {0}/virtualenvs/bcrdata1_11/bin/activate'.format(HOME_DIR)):
            run('python2.7 bcrdata/manage.py migrate --settings=bcrdatabase.settings.production')

@task
def fix_permissions():
    """Fix permissions of the static files
    """
    with cd(os.path.join(APP_DIR)):
        run('find bcrdata/bcrdatabase -type d -exec chmod go+rx {} \;')
        run('find bcrdata/bcrdatabase -type f -exec chmod go+r {} \;')

@task
def restart_apache():
    """Restart Apache2 on webfaction
    """
    with cd(os.path.join(APP_DIR, 'apache2/bin')):
        run('./restart')

@task
def tag_git():
    """Tag git locally with product tag
    """
    local('git tag -af -m "Production Tag" production')
    #local('git push --tags')
