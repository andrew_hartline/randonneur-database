#!/usr/bin/env bash
     
if [ ! -f "DATABASE_IS_MIGRATED.txt" ]; then
   echo "Initial Startup: Performing database migrations..."
   echo
   python ./manage.py migrate && \
   touch DATABASE_IS_MIGRATED.txt
   echo "Database migrations complete!"
   echo
   echo
fi

echo "Starting database server..."
python ./manage.py runserver 0.0.0.0:8000