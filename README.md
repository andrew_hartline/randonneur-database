# README #

Randonneuring Results Database  
This application is currently running at https://database.randonneurs.bc.ca

### How do I get set up? ###

Clone the repository
```shell
$ git clone git@bitbucket.org:bcrandonneurs/randonneur-database.git
```

**If you want linting to work**, ensure you have `pylint`, `pylint-django` installed as well as `django`  
e.g. `pip install Django==1.11`

#### Docker

Install the [appropriate Docker install for your OS](https://docs.docker.com/engine/installation/#supported-platforms).  
Ensure that the cloned repository location is available to docker (OSX: Docker -> Preferences... -> File Sharing)  

Grab a dump of the database and put it into a local folder entitled `local-db/dump`. You can put the media in `local-db/media`.

Then run `docker-compose up` in your local repository. 
This should spin up a mysql container, load the dump, then spin up the database

### Filing Issues ###

Please review open issues at https://bitbucket.org/bcrandonneurs/randonneur-database/issues?status=new&status=open and file a new one if your issue does not appear there.


### Who do I talk to? ###

* Contact Ryan Golbeck (gowlin@gmail.com)
* Contact Étienne Hossack (rfc2324c418@gmail.com)

### How to deploy ###

1. Install fabric3 `pip install fabric3`
2. Ensure you're able to access the production website
3. Ensure you're in the project root _and_ your root is name `bcrdata` -> Yes the folder name matters
4. Run `fab deploy`