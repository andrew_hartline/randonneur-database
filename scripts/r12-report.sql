use bcrandonneur_dat;

create temporary table yearlyResults
select events.eventid, type, rideresults.memberid, lastname, firstname, year(date) as year, month(date) as month,
       concat("https://database.randonneurs.bc.ca/event/", convert(events.eventid, char)) as url from rideresults join events join members
where rideresults.eventid = events.eventid and rideresults.memberID = members.memberid
  and events.date > '2008-01-01'
  and time > 0
  and official = 1
group by memberid, year, month
order by lastname, firstname, date
;

create temporary table removemembers
select memberid, count(eventid) as eventcount from yearlyResults
group by memberid;

delete from removemembers where eventcount > 11;

delete yearlyResults from yearlyResults join removemembers
where yearlyResults.memberid = removemembers.memberid;

select * from yearlyResults;

drop table yearlyResults;

#select events.eventid, type, rideresults.memberid, lastname, firstname, year(date) as year, month(date) as month,
#       concat("https://database.randonneurs.bc.ca/event/", convert(events.eventid, char)) as url from rideresults join events join members
#where rideresults.eventid = events.eventid and rideresults.memberID = members.memberid
#  and events.date > '2011-01-01'
#  and time > 0
#  and official = 1
#group by memberid, year, month
#order by lastname, firstname, date
#;