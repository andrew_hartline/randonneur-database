import sys
import MySQLdb
import csv
import time

homolFile = eventID = sys.argv[1]

h_input = csv.reader(open(homolFile))

db = MySQLdb.connect(host='localhost', user='bcrandonneur_dat', passwd='bcrandoyo', db='bcrandonneur_dat')

while True:
    try:
        h_input.next()  # Unusable headers
    except csv.Error, e:
        print "Done!"
        db.close()

    event_info = h_input.next();
    acpclub = event_info[4]
    eventtime = time.strptime(event_info[5], '%Y-%m-%d')
    eventdate = time.strftime("%Y-%m-%d", eventtime)
    eventdistance = event_info[6].split()[0]

    print "Processing homologation for a " + eventdistance + " brevet on " + eventdate + " in region " + acpclub
    print "Attempting to identify event id..."

    eventid_query = "select eventID from events where distance = " + eventdistance + " AND date(date) = '" + eventdate + "' AND acpRegion = " + acpclub
    print "EventID search string: " + eventid_query
    eventid_cursor = db.cursor()
    eventid_cursor.execute(eventid_query)

    print "Found " + str(eventid_cursor.rowcount) + " events"

    if eventid_cursor.rowcount != 1:
        print "ERROR: Cannot determine unique event.  Bailing out."
        exit

    eventrow = eventid_cursor.fetchone()
    eventID = eventrow[0]

    eventid_cursor.close()

    print "EventID: " + str(eventID)

    h_input.next() # Unusable headers

    for row in h_input:
        if row[0] == '' and row[1] == '' and row[2] == '' and row[3] == '':
            break
        acpNum = row[0]
        lastname = row[1]
        firstname = row[2]
        clubname = row[3]
        # Blank
        homeclub = row[5]
        resulttime = row[6]
        medal = row[7]
        gender = row[8]
        memberid_query = "select members.memberID from members join rideresults where members.memberID = rideresults.memberID AND " + "rideresults.eventID = " + str(eventID) + " AND members.lastname = '" + lastname + "' AND members.firstname = '" + firstname + "'"
        memberid_cursor = db.cursor()
        memberid_cursor.execute(memberid_query)
        if memberid_cursor.rowcount != 1:
            print "Cannot determine unique member for ", firstname, lastname
        else:
            member_row = memberid_cursor.fetchone()
            memberID = member_row[0]
            memberid_cursor.close()
            #print "MemberID is", memberID, "for", firstname, lastname
            update_query = "update rideresults set acpNumber = " + acpNum + " where eventID = " + str(eventID) + " and memberID = " + str(memberID)
            print update_query
            update_cursor = db.cursor()
            update_cursor.execute(update_query)
            update_cursor.close()
        #print ', '.join(row)
    db.commit()

