# Membership list

use bcrandonneur_dat;

# Rider#, lastname, firstname, gender, address, city, prov, country, postal, evephone, email

# Info Release Members
select memberid, lastname, firstname, gender, address, city, province, country, postalcode, evephone, email, expiredate, `membership-package`, `info-release`
from members
where `info-release` = 1 and expiredate > now()
order by lastname, firstname, memberid;

# Info Non-Release Members
select memberid, lastname, firstname, email
from members
where `info-release` = 0 and expiredate > now()
order by lastname, firstname, memberid;

# BCCC Contributations
select memberid, lastname, firstname, gender, address, city, province, country, postalcode, evephone, email, expiredate, `membership-package`, `info-release`, bcc
from members
where bcc = 1 and  expiredate > now()
order by lastname, firstname, memberid;
