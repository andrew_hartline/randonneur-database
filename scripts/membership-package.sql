use bcrandonneur_dat;

# membership-package to send
#select memberID, `membership-package`, lastname, firstname, `info-release`, address, city, province, country, postalcode, email from members where expiredate > now() and (`membership-package` is null or `membership-package` < '2012-01-01') order by lastname, firstname;
select memberID, `membership-package`, lastname, firstname, `info-release`, bcc, address, city, province, country, postalcode, email, expiredate 
from members 
where expiredate > now() 
  and (`membership-package` is null or year(`membership-package`) < year(expiredate)-1) order by lastname, firstname;
# update membership-package
update members set `membership-package` = now() 
where  expiredate > now() 
  and (`membership-package` is null or year(`membership-package`) < year(expiredate)-1) order by lastname, firstname;

#update members set `membership-package` = null;

# All members
#select memberID, `membership-package`, lastname, firstname, `info-release`, address, city, province, country, postalcode, email from members where expiredate 
