import sys
import MySQLdb
import os
import shutil

db = MySQLdb.connect(host='localhost', user='bcrandonneur_dat', passwd='bcrandoyo', db='bcrandonneur_dat')



def timeSplit(t):
    length = len(t)
    ret = t[0:length-2] + " h " + t[length-2:length]
    return ret

def generateCSV(eventID, output_file):

    results_query = "select members.lastname, members.firstname, members.gender, rideresults.time, rideresults.medal, members.club, rideresults.official, clubcodes.name, rideresults.acpNumber from members join events join rideresults join clubcodes where clubcodes.idclubcodes = members.club AND members.memberID = rideresults.memberID and events.eventID = rideresults.eventID  AND events.eventID = " + eventID + " order by members.lastname"
    event_query = "select acpRegion, date, distance from events where eventID = " + eventID

    # Fetch event informations.
    eventcode_cursor = db.cursor()
    eventcode_cursor.execute(event_query)
    eventcode_results = eventcode_cursor.fetchone()
    eventcode = eventcode_results[0]
    eventdate = eventcode_results[1]
    eventdistance = eventcode_results[2]

    # Fetch club codes and names
    clubcodes_query = "select idclubcodes, name from clubcodes where idclubcodes = " + eventcode
    clubcodes_cursor = db.cursor()
    clubcodes_cursor.execute(clubcodes_query)

    # Fetch result informations.
    results_cursor = db.cursor()
    results_cursor.execute(results_query)
    numrows = int(results_cursor.rowcount)

    # Output Header information
    bccode = clubcodes_cursor.fetchone()

    ### Homologation header

    # RUSA CSV Format
    #print str(bccode[1]) + "," +  str(eventcode) + "," + eventdate.strftime("%d/%m/%y") + "," + str(eventdistance)

    # Excel BCR Format
    output_file.write("N Homologation,CLUB ORGANISATEUR,,,code ACP,DATE,DISTANCE,INFORMATIONS,\n")
    output_file.write("," + str(bccode[1]) + ",,," + str(eventcode) + "," + eventdate.strftime("%d/%m/%y") + "," + str(eventdistance) + " km,Medaille,Sexe\n")
    output_file.write(",NOM,PRENOM,CLUB DU PARTICIPANT,,CODE ACP,TEMPS,(x),(F)\n")

    printed_non_official = 0

    for x in range(0,numrows):
        row = results_cursor.fetchone()
        lastname = row[0]
        firstname = row[1]
        if row[2] == 'F': 
            gender = row[2]
        else:
            gender = ""
        if row[3] == -1:  # DNF
            continue
        time = timeSplit(str(row[3]))
        if str(row[4]) == "0":
            medal = ""
        else: 
            medal = "x"
        
        memberclubnum = row[5]
        memberclubname = row[7]
        if memberclubnum == "011611" or memberclubnum == "011600" or memberclubnum == "011602" or memberclubnum == "011621" or memberclubnum == "011641":
            memberclubnum = "011600"
            memberclubname = "British Columbia Randonneurs Cycling Club" 

        official = row[6]
        if not official and not printed_non_official:
            printed_non_official = 1
            sys.stderr.write("WARNING: Some results not official.\n")
        acpNumber = row[8]
        if acpNumber != None:
            sys.stderr.write("WARNING: Some results have acpNumbers.\n")
            continue

        # RUSA CSV Format
        #print lastname + "," + firstname + "," + memberclubname + "," + memberclubnum + "," + time + "," +  medal + "," + gender

        # BCR Excel Format
        output_file.write("," + lastname + "," + firstname + "," + memberclubname + ",,\"" + memberclubnum + "\"," + time + "," + medal + "," + gender + "\n")

    output_file.write("\n")


def findUnHomolEvents():
    query = "SELECT events.eventID, events.distance, events.date FROM events LEFT JOIN rideresults ON (events.eventID = rideresults.eventID) WHERE date > '2010-01-01' AND date < now() AND (events.acpRegion = '011631' or events.acpRegion = '011621' or events.acpRegion = '011602' or events.acpRegion = '011641' or events.acpRegion = '011611' or events.acpRegion = '011600') AND (type = 'RM' OR type = 'ACPB') AND time > 0 AND acpNumber is NULL AND rideresults.official = 1 GROUP BY events.eventID;"
    
    query_cursor = db.cursor()
    query_cursor.execute(query)
    return query_cursor.fetchall()


shutil.rmtree('tmp')
os.mkdir('tmp')

for event in findUnHomolEvents():
    eventID = event[0]
    distance = event[1]
    date = event[2]
    print "Generating homolgation for a " + str(distance) + " Brevet (" + str(eventID) + ") on " + str(date)
    f = open("tmp/" + str(distance) + ".csv", 'a')
    generateCSV(str(eventID), f)
    f.close()

