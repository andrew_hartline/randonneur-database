from django.conf.urls import include, url
from bcrdatabase.api.views import member_status_by_name, member_status_by_email, member_by_id, members, route_by_id, routes, event_by_id, events, results_by_id, results, permanents_by_year

urlpatterns = [url(r'member/(?P<last_name>\w+)/(?P<first_name>\w+)/status/$', member_status_by_name),
               url(r'member/(?P<email>[^@]+@[^@]+\.[^@]+)/status$', member_status_by_email),
               url(r'members/(?P<member_id>\d+)$', member_by_id),
               url(r'members', members),
               url(r'routes/(?P<id>\d+)$', route_by_id),
               url(r'routes', routes),
               url(r'events/(?P<id>\d+)$', event_by_id),
               url(r'events', events),
               url(r'results/(?P<id>\d+)$', results_by_id),
               url(r'results', results),
               url(r'permanents/(?P<year>\d+)$', permanents_by_year)
]
