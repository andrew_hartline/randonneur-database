from django.test import TestCase
from django.test.client import Client

class ApiEventTest(TestCase):

    def test_api_events_succeeds(self):
        response = self.client.get('/api/events')
        self.assertEquals(response.status_code, 200)

    def test_api_event_succeeds(self):
        response = self.client.get('/api/events/1')
        self.assertEquals(response.status_code, 200)
