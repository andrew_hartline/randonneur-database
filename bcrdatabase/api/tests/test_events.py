import unittest
import json
from django.test import Client, TestCase
from bcrdatabase.bcrdata.models import Event

from bcrdatabase.bcrdata.tests.Mocks import MockEvents

class EventsApi(TestCase):
    """Tests for the Event REST API"""

    def setUp(self):
        self.event = MockEvents.create()
        self.event = Event.objects.get(pk=self.event.eventid)

    def retrieveEvent(self):
        return self.client.get('/api/events/' + str(self.event.eventid) + '/')

    def test_eventNotFound(self):
        """Test event not found"""
        response = self.client.get('/api/events/1000')
        self.assertEqual(response.status_code, 404)

    def test_eventFound(self):
        """Test found event"""
        response = self.retrieveEvent()
        self.assertEqual(response.status_code, 200)

    def test_eventHasWebData(self):
        """Test event has required web data"""
        response = self.retrieveEvent()
        event_json = json.loads(response.content)['events'][0]
        self.assertEqual(event_json['distance'], self.event.distance)
        self.assertEqual(event_json['name'], self.event.name)
        self.assertEqual(event_json['acpregion']['displayname'], self.event.name)
        self.assertEqual(event_json['date'], str(self.event.date.year) + "-" + str(self.event.date.month) + "-" + str(self.event.date.day))
        self.assertEqual(event_json['start'], self.event.start)
        #self.assertEqual(event_json['datetime'], str(self.event.date))
        self.assertEqual(event_json['ridegroup'], self.event.ridegroup)
        self.assertEqual(event_json['route']['name'], self.event.routeid.name)
        self.assertEqual(event_json['volunteers'], [])
        self.assertEqual(event_json['results'], [])


