import json
from django.http import HttpResponse
from bcrdatabase.bcrdata.models import Member, Event, Route, Rideresult, PermanentEvent
from django.core import serializers
from django.shortcuts import get_object_or_404

def member_array_to_response(members):
    size = members.count()
    if size == 1:
        member = members[0]
        response = { 'lastname' : member.lastname,
                     'firstname' : member.firstname,
                     'is_current_member' : member.is_current_member(),
                     'member_id' : member.memberid,
                     'waiver': member.waiver,
#                     'email' : member.email,
                     }
    else:
        response = {} 

    return response

def member_status_by_name(request, first_name, last_name):
    members = Member.objects.filter(lastname=last_name,firstname=first_name)
    response = member_array_to_response(members)
    return HttpResponse(json.dumps(response), content_type='application/json')

def member_status_by_email(request, email):
    members = Member.objects.filter(email=email)
    response = member_array_to_response(members)
    return HttpResponse(json.dumps(response), content_type='application/json')

def member_by_id(request, member_id):
    m = Member.objects.get(pk=member_id)
    member = { 'member' : m.to_dict(),
               'results' : map(lambda r: r.to_dict(), m.getBrevetResults()) }
    return HttpResponse(json.dumps(member), content_type='application/json')

def members(request):
    if "ids[]" in request.GET:
        ids = map(lambda s : int(s), request.GET.getlist('ids[]'))
        members = Member.objects.filter(memberid__in=ids).order_by('lastname', 'firstname')
    else:
        members = Member.objects.order_by('lastname', 'firstname')
    members_dict = { 'members' : map(lambda m: m.to_dict(), members) }
    return HttpResponse(json.dumps(members_dict), content_type='application/json')

def events(request):
    if "ids[]" in request.GET:
        ids = map(lambda s: int(s), request.GET.getlist('ids[]'))
        events = Event.objects.filter(eventid__in=ids)
    else:
        events = Event.objects.all()
    if "year" in request.GET:
        events = events.filter(date__year=request.GET['year'])
    if "acpregion" in request.GET:
        events = events.filter(acpregion__idclubcodes=request.GET['acpregion'])
    events = events.order_by('-date').exclude(type='BCP')
    expand = True
    if "expand" in request.GET and request.GET['expand'] == 'false':
        expand = False
    events_dict = { 'events' : map(lambda e : e.to_dict(expand), events) }
    return HttpResponse(json.dumps(events_dict), content_type='application/json')

def event_by_id(request, id):
    expand = True
    if "expand" in request.GET and request.GET['expand'] == 'false':
        expand = False
    event = { 'event' : get_object_or_404(Event,pk=id).to_dict(expand) }
    return HttpResponse(json.dumps(event), content_type='application/json')

def permanents_by_year(request, year):
    results = PermanentEvent.objects.filter(date__year=year).order_by('-date')
    results_dict = { 'results' : map(lambda r: r.to_dict(), results) }
    return HttpResponse(json.dumps(results_dict), content_type='application/json')

def results(request):
    if "ids[]" in request.GET:
        ids = map(lambda s: int(s), request.GET.getlist('ids[]'))
        results = Rideresult.objects.filter(rideresultid__in=ids)
    else:
        results = Rideresult.objects.all()
    results_dict = { 'results' : map(lambda r: r.to_dict(), results) }
    return HttpResponse(json.dumps(results_dict), content_type='application/json')

def results_by_id(request, id):
    result = { 'result' : Rideresult.objects.get(pk=id).to_dict()}
    return HttpResponse(json.dumps(result), content_type='application/json')

def routes(request):
    if "type" in request.GET and request.GET['type'] == "permanent":
        routes = Route.objects.filter(permanentid__isnull=False).order_by('permanentid')
    else:
        routes = Route.objects.all()
    results_dict = map(lambda r: r.to_dict(), routes)
    return HttpResponse(json.dumps(results_dict), content_type='application/json')

def route_by_id(request, id):
    route = { 'route' : Route.objects.get(pk=id).to_dict()}
    return HttpResponse(json.dumps(route), content_type='application/json')
