from django.conf.urls import include, url
from django.conf import settings
from django.conf.urls.static import static
from bcrdatabase.registration import views as registration_views

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = [
    url(r'', include('bcrdatabase.bcrdata.urls')),

    # BCR Apps
    url(r'^schedule/', include('bcrdatabase.schedule.urls')),     
    url(r'^reporting/', include('bcrdatabase.reporting.urls')),
    url(r'^search/', include('bcrdatabase.search.urls')),
    url(r'^management/', include('bcrdatabase.management.urls')),

    # JSON API
    url(r'^api/', include('bcrdatabase.api.urls')),

    # Selectable
    url(r'^selectable/', include('selectable.urls')),

    # Admin apps
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),

    # Login, Logout
    url(r'', include('bcrdatabase.registration.urls')),

    # Markdown editor
    url(r'^markdown/', include('django_markdown.urls')),

    # Favicon
    url(r'^', include('favicon.urls')),
]



# ... the rest of your URLconf goes here ...

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
