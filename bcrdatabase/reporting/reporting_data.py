"""Queries specific to various reports
"""

from django.utils import timezone
from bcrdatabase.bcrdata.models import Clubcode, Rideresult, Event

DISTANCES = [200, 300, 400, 600, 1000, 1200, 2000]

UNKNOWN_MEMBER = '1770'

GENERIC_BC_REGION = '011600'


def bc_regions():
    """Specifically returns regions for BC Randonneurs"""
    return Clubcode.objects.filter(country='CA', province='BC')


def canadian_regions():
    return Clubcode.objects.filter(country='CA')


def get_first_year():
    """get the first year for any result"""
    try:
        earliest_result = Rideresult.objects.order_by('eventid__date')[
            0:1].get()
        return earliest_result.eventid.date.year
    except Rideresult.DoesNotExist:
        return timezone.now().year


def get_last_year():
    """get the last year for any event"""
    try:
        latest_event = Event.objects.order_by('-date')[0:1].get()
        return latest_event.date.year
    except Event.DoesNotExist:
        return timezone.now().year


def get_year_list():
    """gets the list of years we know about as strings"""
    return map(str, range(get_last_year(), get_first_year(), -1))
