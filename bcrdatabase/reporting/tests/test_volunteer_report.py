# pylint: skip-file
from datetime import datetime
from collections import namedtuple
import unittest
from django.test import Client, TestCase
from fluent_url_assert import UrlAssert

import bcrdatabase.reporting.reports.volunteer_report as report

TEST_DATE = datetime.today()

class VolunteerReport(TestCase):

    def assertUrl(self, url):
        return UrlAssert(self, url)

    def test_loads(self):
        self.assertUrl('/reporting/volunteer-report/').shouldYield(200)

    def test_loads_year(self):
        self.assertUrl('/reporting/volunteer-report/2017').shouldYield(200, 'for a given year')

    def test_no_trailing_slash(self):
        self.assertUrl('/reporting/volunteer-report/2017/').shouldYield(404, 'the year is not a path')

    def test_loads_year_unknown(self):
        self.assertUrl('/reporting/volunteer-report/3001/').shouldYield(404, 'invalid year')

    def test_loads_grouping(self):
        self.assertUrl('/reporting/volunteer-report/2017?group_by_region=true').shouldYield(200, 'grouping works')

    def test_loads_invalid_grouping(self):
        self.assertUrl('/reporting/volunteer-report/2017?group_by_region=12') \
            .shouldYield(200, 'grouping works with invalid value')

    def test_is_fleche_populaire(self):
        self.assertFalse(report._is_populaire(self.mockBrevet()))
        self.assertTrue(report._is_populaire(self.mockPop()))

        self.assertFalse(report._is_fleche(self.mockPop()))
        self.assertFalse(report._is_fleche(self.mock("Event",{'name':None})))
        self.assertTrue(report._is_fleche(self.mockFleche()))

    def test_valid_for_report(self):
        """test report._valid_for_report(event, event_volunteers, event_rider_count):"""
        self.assertFalse(report._valid_for_report(self.mockBrevet(), [], 0), 'No volunteers')
        self.assertFalse(report._valid_for_report(self.mockBrevet(), ['vol'], 1), 'No riders/one rider')

        self.assertTrue(report._valid_for_report(self.mockBrevet(), ['vol'], 2), 'Riders')
        self.assertTrue(report._valid_for_report(self.mockPop(), ['vol'], 0), 'Populaire')
        self.assertTrue(report._valid_for_report(self.mockPop(), ['vol'], 0), 'Fleche')

    def test_get_data(self):
        """test report._get_data(existing_data, volunteer_result, event_riders=0):"""
        data = report._get_data(None, self.mockOrg())
        self.assertData(data)

        data = report._get_data(None, self.mockOrg(), 10)
        self.assertData(data)

        duplicate = report._get_data(data, self.mockOrg())
        self.assertData(duplicate)

        anotherEvent = self.mock('Event', {'type':'ACPB','name':'brevet2','eventid':20,
                                           'acpregion':self.mockLm(),'date':TEST_DATE})
        events = data.events[:]
        events.append({'eventid':20,'did_organize':True,'name':'brevet2','date':TEST_DATE})
        another = report._get_data(data, self.mockOrg(event=anotherEvent))
        self.assertData(another, points=5, events=events)

        anotherEventSameName = self.mock('Event', {'type':'ACPB','name':'brevet','eventid':21,
                                           'acpregion':self.mockLm(),
                                           'date':datetime(2012, 9, 16, 0, 0)})
        events.append({'eventid':21,'did_organize':True,'name':'brevet (2012)',
                       'date':anotherEventSameName.date})
        anotherData = report._get_data(another, self.mockOrg(event=anotherEventSameName))
        self.assertData(anotherData, points=7.5, events=events)

        genericClub = self.mock("Clubcode", {"idclubcodes":'011600'})
        genericMember = self.mock("Member", {'club':genericClub,'firstname':'An','lastname':'Organizer','memberid':42})
        guessRegionResult = self.mockOrg(member=genericMember)
        data = report._get_data(None, guessRegionResult)
        self.assertData(data, guessed_region=True)

    def assertData(self, data, name='An Organizer',
                               memberid=42,
                               region_id='011602',
                               guessed_region=False,
                               points=2.5,
                               events=[{'eventid':42,'did_organize':True,'name':'brevet','date':TEST_DATE}]):
        self.assertEqual(data.name, name)
        self.assertEqual(data.memberid, memberid)
        self.assertEqual(data.region_id, region_id)
        self.assertEqual(data.guessed_region, guessed_region)
        self.assertEqual(data.points, points)
        self.assertEqual(data.events, events)
        self.assertEqual(data.event_count, len(events))

    def test_small_populaire(self):
        event = self.mock('Event',{'name':'popular'})
        self.assertFalse(report._small_populaire(event, 0), "Not enough data")
        self.assertFalse(report._small_populaire(event, 100), "Big")
        self.assertTrue(report._small_populaire(event, 20), "20 is small")
        event = self.mock('Event',{'name':'peace is less popular'})
        self.assertTrue(report._small_populaire(event, 0), "Peace region")

    def test_get_event_name(self):
        self.assertEquals(report._get_event_name([], self.mock('Event', {'name':None})), '(Unknown)')
        self.assertEquals(report._get_event_name([], self.mock('Event', {'name':''})), '(Unknown)')

    def test_points(self):
        """test report._calculate_points(base_points, volunteer_result, multiple_distances, event_riders)"""
        result = self.mock("Volunteer", {'organizer':False})
        points = report._calculate_points(0, result, False, 0)
        self.assertEqual(points, 1, 'only one if not organize: '+str(points))

        points = report._calculate_points(1, result, False, 0)
        self.assertEqual(points, 2, 'add points: '+str(points))

        result = self.mockOrg()
        points = report._calculate_points(0, result, True, 0)
        self.assertEqual(points, 2.5, 'organizer gets more points: '+str(points))

        points = report._calculate_points(0, result, True, 20)
        self.assertEqual(points, 3, 'riders bonus: '+str(points))
        points = report._calculate_points(0, result, True, 25)
        self.assertEqual(points, 3, 'riders bonus: '+str(points))
        points = report._calculate_points(0, result, True, 45)
        self.assertEqual(points, 3.5, 'ruders bonus: '+str(points))

        result = self.mock("Volunteer", {'organizer':True,'eventid':self.mockPop()})
        points = report._calculate_points(0, result, True, 0)
        self.assertEqual(points, 2.5, 'No populaire points if multiple distances: '+str(points))
        points = report._calculate_points(0, result, False, 0)
        self.assertEqual(points, 6, 'populaire points: '+str(points))

        result = self.mock("Volunteer", {'organizer':True,'eventid':self.mockFleche()})
        points = report._calculate_points(0, result, True, 0)
        self.assertEqual(points, 4, 'fleche points: '+str(points))

    def mock(self, class_name, dict):
        return namedtuple(class_name, dict.keys())(*dict.values())

    def mockOrg(self, event=None, member=None):
        mockMember = self.mock("Member", {'club':self.mockLm(),'firstname':'An','lastname':'Organizer','memberid':42})
        return self.mock("Volunteer", { 'organizer': True,
                                        'eventid': self.mockBrevet(self.mockLm()) if not event else event,
                                        'memberid':mockMember if not member else member})

    def mockBrevet(self, mockClub=None):
        base = {'type':'ACPB','name':'brevet','eventid':42,'date':TEST_DATE}
        if mockClub:
            base['acpregion'] = mockClub
        return self.mock("Event", base)

    def mockLm(self):
        return self.mock("Clubcode", {'idclubcodes':'011602'})

    def mockFleche(self):
        return self.mock("Event", {'type':'ACPB','name':'Fleche'})

    def mockPop(self):
        return self.mock("Event", {'type':'BCPop','name':'Popular Event'})