import unittest
from django.test import Client, TestCase

class FastestBrevets(TestCase):
    """Tests for the fastest brevets report"""

    def test_loads(self):
        response = self.client.get('/reporting/fastest-brevets/')
        self.assertEqual(response.status_code, 200)

    def test_loads_distance(self):
        response = self.client.get('/reporting/fastest-brevets/200/')
        self.assertEquals(response.status_code, 200)

    def test_loads_distance_unknown(self):
        response = self.client.get('/reporting/fastest-brevets/2001/')
        self.assertEquals(response.status_code, 404)
