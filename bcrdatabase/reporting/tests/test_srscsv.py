import unittest
from django.test import Client, TestCase
from bcrdatabase.bcrdata.models import Event
from bcrdatabase.bcrdata.tests.Mocks import MockEvents
from datetime import date, timedelta

class SuperRandonneurReport(TestCase):
    """Tests for the Super Randonneur Reports"""

    def test_srs_csv(self):
        """Test the Super Randonneur CSV Report"""
        response = self.client.get('/reporting/sr/1980/csv')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], 'text/csv')
