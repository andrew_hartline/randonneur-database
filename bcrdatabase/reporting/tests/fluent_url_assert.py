# pylint: disable=C0103
"""assertions on urls"""
import json

class UrlAssert:
    """A simple class for more fluent url assertions

    Usage:\n
        in class MyTest(TestCase):\n
            def assertUrl(url):\n
                return UrlAssert(url, self)\n

            def test_something():\n
                self.assertUrl('/').shouldYield(200)\n
                self.assertUrl('/?').shouldYield(500, 'because that is invalid')
    """

    def __init__(self, test, url):
        self.test = test
        self.response = test.client.get(url)

    def shouldYield(self, status, msg=None):
        """performs the assertion with an optional assertion message"""
        fail_msg = json.dumps(self.response.request)
        if msg:
            self.test.assertEqual(self.response.status_code, status, msg + '\n' + fail_msg)
        else:
            self.test.assertEqual(self.response.status_code, status, fail_msg)
