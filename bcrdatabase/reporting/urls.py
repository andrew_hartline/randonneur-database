"""URL patterns for the reporting module"""

from django.conf.urls import url
from bcrdatabase.reporting.views import reports, volunteer_report
from bcrdatabase.reporting.views import current_members, fastest_brevets
from bcrdatabase.reporting.views import rider_totals, rider_overall_totals, sr, srcsv
from bcrdatabase.reporting.views import yearly_detail, yearly_summary
from bcrdatabase.reporting.views import missing_elevation, missing_names, missing_routes
from bcrdatabase.reporting.views import missing_control

urlpatterns = [url(r'^$', reports),
               url(r'^fastest-brevets/$', fastest_brevets, name='fastest_brevets'),
               url(r'^fastest-brevets/(?P<distance>\d+)/$',
                   fastest_brevets,
                   name='fastest_brevets_distance'),
               url(r'^volunteer-report/$', volunteer_report, name='volunteer_report'),
               url(r'^volunteer-report/(?P<year>(\d+|all))$',
                   volunteer_report,
                   name='volunteer_report'),
               url(r'^current-members/$', current_members, name='current_members'),
               url(r'^rider-overall-totals/$', rider_overall_totals, name='rider_overall_totals'),
               url(r'^rider-totals/$', rider_totals, name='rider_totals'),
               url(r'^rider-totals/(?P<year>\d+)/$', rider_totals),
               url(r'^yearly_detail/$', yearly_detail, name='yearly_detail'),
               url(r'^yearly_detail/(?P<year>\d+)/$', yearly_detail, name='yearly_detail'),
               url(r'^yearly_summary/$', yearly_summary, name='yearly_summary'),
               url(r'^yearly_summary/(?P<year>\d+)/$', yearly_summary, name='yearly_summary'),
               url(r'^sr/$', sr, name='cur_sr'),
               url(r'^sr/(?P<year>\d+)/$', sr, name='sr'),
               url(r'^sr/(?P<year>\d+)/csv$', srcsv, name='srcsv'),
               url(r'^missing_elevation/$', missing_elevation, name='routes_missing_elevation'),
               url(r'^missing_names/$', missing_names, name='events_missing_names'),
               url(r'^missing_routes/$', missing_routes, name='events_missing_routes'),
               url(r'^missing_control/$', missing_control, name='events_missing_control'),
              ]
