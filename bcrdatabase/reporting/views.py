import csv
from datetime import datetime

import bcrdatabase.reporting.reporting_data as reporting_data
import bcrdatabase.reporting.reports as db_reports
import bcrdatabase.reporting.sr as super_randonneurs
from bcrdatabase.bcrdata.models import (Event, Member, Rideresult, Route,
                                        filterForACPYear, filterForBrevets,
                                        filterForFinished, filterUnknown,
                                        filterUnofficial)
from django.db.models import Count, Q, Sum
from django.http import Http404, HttpResponse
from django.shortcuts import get_object_or_404, render
from django.template import Context, RequestContext, loader
from django.utils import timezone
from django.views.decorators.cache import cache_page

"""The views for all major reports"""


def reports(request):
    return render(request, 'reporting/index.html')


@cache_page(60*60*24)
def fastest_brevets(request, distance=None):
    if distance == None:
        return render(request,
                      'reporting/fastest-brevets.html',
                      {'fastest_times': db_reports.fastest_brevets.get_fastest_times()})
    else:
        if db_reports.fastest_brevets.no_report_for_distance(int(distance)):
            raise Http404('No such distance: ' + str(distance))

        return render(request,
                      'reporting/fastest-brevets-distance.html',
                      {'distance': distance,
                       'results': db_reports.fastest_brevets.for_distance(distance)})


@cache_page(60*60*24)
def yearly_detail(request, year=timezone.now().year):
    results = db_reports.end_of_year.calculate_results(int(year))
    return render(request, 'reporting/yearly_detail.html', {'riders': results, 'year': int(year)})


@cache_page(60*60*24)
def volunteer_report(request, year=timezone.now().year):
    year = int(year) if year != 'all' else -1
    return render(request,
                  'reporting/volunteer_report.html',
                  {'year': str(year),
                   'year_list': reporting_data.get_year_list(),
                   'volunteer_list': db_reports.volunteer_report.get_totals(year),
                   'group_by_region': request.GET.get('group_by_region') == 'true'})


def missing_names(request):
    event_results = Event.objects.filter(name="").order_by('date')

    return render(request, 'reporting/events-missing-names.html',
                  {'page_name': 'Events Missing Names',
                   'event_results': event_results})


def missing_elevation(request):
    route_results = Route.objects.filter(climbing__isnull=True)
    recent_events = _get_recent_events(route_results)

    return render(request, 'reporting/routes-missing-elevation.html',
                  {'page_name': 'Routes Missing Elevations',
                   'route_results': route_results,
                   'latest_event_results': recent_events})


def _get_recent_events(routes):
    recent_events = []
    for route in routes:
        recent_result = Event.objects.filter(
            routeid=route.routeid).order_by('date')[:1]
        recent_events.append(None if not recent_result else recent_result[0])

    return recent_events


def missing_routes(request):
    unknownRoute = get_object_or_404(Route, pk=100)
    event_results = Event.objects.filter(routeid=unknownRoute).order_by('date')

    return render(request, 'reporting/events-missing-routes.html',
                  {'page_name': 'Events Missing Routes',
                   'event_results': event_results})


def missing_control(request):
    query = Q(controlcard__isnull=True) | \
        Q(routesheet__isnull=True) | \
        Q(controlcard="") | \
        Q(routesheet="")
    event_results = Event.objects.filter(query).order_by('date')

    return render(request, 'reporting/events-missing-control.html',
                  {'page_name': 'Events Missing Control Card or Routesheet',
                   'event_results': event_results})


@cache_page(60*60*24)
def yearly_summary(request, year=timezone.now().year):
    summary = db_reports.yearly_summary.calculate_totals(int(year))

    for total in list(summary.by_region):
        if total['name'] == 'BC Randonneurs' and total['total'] == 0:
            summary.by_region.remove(total)

    for r in summary.by_region:
        r.update((n, "Southern Interior")
                 for n, v in r.iteritems() if v == u" Kamloops")
    for r in summary.by_riders_region:
        r.update((n, "Southern Interior")
                 for n, v in r.iteritems() if v == u" Kamloops")
        r.update((n, "Outside BC")
                 for n, v in r.iteritems() if v == u"BC Randonneurs")

    return render(request, 'reporting/yearly_summary.html', {'summary': summary, 'year': year})


@cache_page(60*60*24)
def sr(request, year=timezone.now().year):
    allSRs = super_randonneurs.get_all_srs()
    return render(request, 'reporting/sr.html',
                  {'all_srs': allSRs,
                   'yearList': reporting_data.get_year_list(),
                   'SRs': allSRs[str(year)],
                   'year': str(year)})


@cache_page(60*60*24)
def srcsv(request, year=timezone.now().year):
    allSRs = super_randonneurs.get_all_srs()
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=SRList-' + \
        str(year) + '.csv'
    template = loader.get_template('reporting/sr.csv')
    template_data = {'all_srs': allSRs,
                     'yearList': reporting_data.get_year_list(),
                     'SRs': allSRs.get(str(year), []),
                     'year': str(year)}
    response.write(template.render(template_data))
    return response


def current_members(request):
    members = Member.objects.filter(expiredate__gte=timezone.now())

    return render(request, 'reporting/current-members.html', {'members': members})


def rider_overall_totals(request):
    brevetresults = filterUnofficial(filterForFinished(Rideresult.objects.exclude(eventid__type='BCP'))) \
        .values('memberid__memberid', 'memberid__lastname', 'memberid__firstname') \
        .annotate(Count('memberid'), Sum('eventid__distance'), Sum('time')) \
        .filter(eventid__distance__sum__gte=5000) \
        .order_by('-eventid__distance__sum')
    allSRs = super_randonneurs.get_all_srs()
    return render(request, 'reporting/rider-overall-totals.html', {'yearResults': brevetresults, 'allSRs': allSRs})


def rider_totals(request, year=timezone.now().year):
    year = int(year)
    brevetresults = filterUnofficial(filterForACPYear(filterForFinished(Rideresult.objects.exclude(eventid__type='BCP')), year)) \
        .values('memberid__memberid', 'memberid__lastname', 'memberid__firstname') \
        .annotate(Count('memberid'), Sum('eventid__distance'), Sum('time')) \
        .order_by('-eventid__distance__sum')
    permresults = filterUnofficial(filterForFinished(Rideresult.objects.filter(eventid__date__year=year).filter(eventid__type='BCP'))) \
        .values('memberid__memberid', 'memberid__lastname', 'memberid__firstname') \
        .annotate(Count('memberid'), Sum('eventid__distance'), Sum('time')) \
        .order_by('-eventid__distance__sum')

    return render(request, 'reporting/rider-totals.html',
                  {'brevet_results': brevetresults,
                   'permanent_results': permresults,
                      'year': str(year),
                      'year_list': reporting_data.get_year_list()})
