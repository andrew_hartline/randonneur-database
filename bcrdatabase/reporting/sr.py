"""
Reporting methods for manipulating finding super randonneurs
"""

from collections import deque, OrderedDict
from sets import Set

from django.core.cache import cache
from django.utils import timezone

from bcrdatabase.bcrdata.models import Event
from bcrdatabase.bcrdata.models import filterForFinished, filterUnofficial
import bcrdatabase.reporting.reporting_data as reporting_data


class SuperRandonneur:
    """Simple Data model for SR data"""

    def __init__(self):
        self.member = None
        self.b200 = None
        self.b300 = None
        self.b400 = None
        self.b600 = None
        self.year = None
        self.firstyear = False

        self._distances = {
            200: [],
            300: [],
            400: [],
            600: []
        }

    def append_result(self, event, result):
        """Adds the result to the stored event list"""
        self._distances[event.distance].append(result)

    def has_done_a_series(self):
        """Determines if the SR is legit or not"""
        return self._done_a(200) and self._done_a(300) \
            and self._done_a(400) and self._done_a(600)

    def _done_a(self, dist):
        return len(self._distances.get(dist, [])) > 0

    def set_first_brevets(self):
        """Take the first found brevet for the SR calculation"""
        self.b200 = self._distances[200][0]
        self.b300 = self._distances[300][0]
        self.b400 = self._distances[400][0]
        self.b600 = self._distances[600][0]


def get_all_srs():
    """computes all srs done, EVER"""

    all_super_randonneurs = cache.get('all_srs')
    if all_super_randonneurs is None:
        all_super_randonneurs = _compute_all_srs()
        cache.set('all_srs', all_super_randonneurs, 60*60*24)
    return all_super_randonneurs


def _compute_all_srs():
    first_known_year = reporting_data.get_first_year()
    next_year = timezone.now().year + 1
    year_map = {}
    multi_srs = Set([])

    for year in range(first_known_year, next_year):
        key = str(year)
        year_map[key] = _compute_for_year(multi_srs, year)

    return year_map


def get_by_member(all_srs, member):
    """Returns all series that have been completed by a given member"""
    srs_by_member = deque([])
    for sr_by_year in all_srs:
        for randonneur in all_srs[sr_by_year]:
            if randonneur.member.memberid == member:
                srs_by_member.append(randonneur)
    return srs_by_member


def _compute_for_year(multi_srs, year):
    srs = deque([])
    by_member = {}

    events = _get_events_for_year(year)

    for event in events:
        _append_results(by_member, year, event)

    members = OrderedDict(
        sorted(by_member.items(), key=lambda m: m[1].member.lastname))
    for member_id in members:
        potential_sr = by_member[member_id]

        if potential_sr.has_done_a_series():

            if _is_first_series(multi_srs, potential_sr):
                potential_sr.firstyear = True
                multi_srs.add(potential_sr.member)

            potential_sr.set_first_brevets()
            srs.append(potential_sr)

    return srs


def _get_events_for_year(year):
    return Event.filterACPBrevetsInYear(year) \
        .filter(distance__in=[200, 300, 400, 600]) \
        .order_by('eventid')


def _append_results(srs_by_member, year, event):
    event_results = filterForFinished(filterUnofficial(event.getResults()))

    for result in event_results:
        member_id = result.memberid.memberid
        if member_id not in srs_by_member:
            srs_by_member[member_id] = SuperRandonneur()
            srs_by_member[member_id].member = result.memberid
            srs_by_member[member_id].year = year

        srs_by_member[member_id].append_result(event, result)


def _is_first_series(multi_srs, randonneur):
    return randonneur.member not in multi_srs
