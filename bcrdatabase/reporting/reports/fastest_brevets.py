"""Calculates the brevets with the least amount of time"""

from bcrdatabase.bcrdata.models import Rideresult
import bcrdatabase.reporting.reporting_data as reporting_data

def no_report_for_distance(distance):
    """determines if the distance is invalid"""
    return int(distance) not in reporting_data.DISTANCES

def get_fastest_times():
    """get fastest times for all defined distances"""
    return map(fastest_time, reporting_data.DISTANCES)

def fastest_time(distance):
    """get fastest time for specified distance"""
    fastest_times = []
    fastest_results = _top_results(distance)

    fastest_results_men = fastest_results.filter(memberid__gender='M')
    fastest_results_women = fastest_results.filter(memberid__gender='F')
    fastest_results_nonbinary = fastest_results.filter(memberid__gender='X')

    if fastest_results_men.exists():
        time = fastest_results_men[0].time
        index = 0
        while fastest_results_men[index].time == time:
            result = fastest_results_men[index]
            fastest_times.append({'distance': distance,
                                  'time': result.formattime,
                                  'rider': result.memberid,
                                  'date': result.eventid.date,
                                  'event': result.eventid})
            index += 1

    if fastest_results_women.exists():
        time = fastest_results_women[0].time
        index = 0
        while fastest_results_women[index].time == time:
            result = fastest_results_women[index]
            fastest_times.append({'distance': distance,
                                  'time': result.formattime,
                                  'rider': result.memberid,
                                  'date': result.eventid.date,
                                  'event': result.eventid})
            index += 1
    
    if fastest_results_nonbinary.exists():
        time = fastest_results_nonbinary[0].time
        index = 0
        while fastest_results_nonbinary[index].time == time:
            result = fastest_results_nonbinary[index]
            fastest_times.append({'distance': distance,
                                  'time': result.formattime,
                                  'rider': result.memberid,
                                  'date': result.eventid.date,
                                  'event': result.eventid})
            index += 1

    return fastest_times

def for_distance(distance):
    """Calculates the top 15 results for a distance"""
    return _top_results(distance).filter(memberid__gender='F')[:10].union(_top_results(distance).filter(memberid__gender='M')[:10], _top_results(distance).filter(memberid__gender='X')[:10])

def _top_results(distance):
    return Rideresult.objects.filter(eventid__distance=distance, time__gt=0) \
                             .order_by('time', 'eventid__date', 'memberid__gender')
