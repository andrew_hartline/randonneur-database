"""Gets all volunteer results members based on year"""

from bcrdatabase.bcrdata.models import Volunteer, Rideresult
from bcrdatabase.bcrdata.models import getBCScheduledEvents
from bcrdatabase.reporting.reporting_data import UNKNOWN_MEMBER, GENERIC_BC_REGION

class VolunteerRow:
    """display class for report page"""

    def __init__(self, memberid):
        self.name = memberid.firstname + ' ' + memberid.lastname
        self.name = self.name.title()
        self.memberid = memberid.memberid
        self.region_id = None
        self.guessed_region = False
        self.points = 0
        self.events = []
        self.event_count = 0

    def __str__(self):
        return 'VolunteerRow({})'.format(', '.join("%s=%s" % item for item in vars(self).items()))

def get_totals(year):
    """Calculates the volunteer totals"""
    volunteers = {}

    if year is not -1:
        events = getBCScheduledEvents().filter(date__year=year).order_by('date')
    else:
        events = getBCScheduledEvents().order_by('date')

    for event in events:
        event_volunteers = Volunteer.objects.filter(eventid=event.eventid)
        event_rider_count = Rideresult.objects.filter(eventid=event.eventid).count()

        if not _valid_for_report(event, event_volunteers, event_rider_count):
            continue

        for result in event_volunteers:
            volunteer_id = result.memberid.memberid
            if str(volunteer_id) == UNKNOWN_MEMBER:
                continue
            volunteers[volunteer_id] = _get_data(volunteers.get(volunteer_id),
                                                 result,
                                                 event_rider_count)

    return sorted(volunteers.values(), key=lambda v: v.points, reverse=True)

def _valid_for_report(event, event_volunteers, event_rider_count):
    has_any_volunteers = len(event_volunteers) > 0
    at_least_some_results = event_rider_count > 1
    no_results_but_reportable = _is_populaire(event) or _is_fleche(event)

    if has_any_volunteers:
        return at_least_some_results or no_results_but_reportable
    else:
        return False

def _is_populaire(event):
    return event.type == 'BCPop'

def _is_fleche(event):
    return event.name is not None and 'fleche' in event.name.lower()

def _get_data(existing_data, volunteer_result, event_riders=0):
    data = VolunteerRow(volunteer_result.memberid)

    events = existing_data.events if existing_data else []
    if _duplicate_event(events, volunteer_result.eventid):
        return existing_data

    multiple_distances = _find_multiples(events, volunteer_result.eventid)
    if not multiple_distances:
        event_name = volunteer_result.eventid.name
        events.append({'did_organize': volunteer_result.organizer,
                       'eventid': volunteer_result.eventid.eventid,
                       'date': volunteer_result.eventid.date,
                       'name': _get_event_name(events, volunteer_result.eventid)})
    data.events = events
    data.event_count = len(events)

    data.region_id = volunteer_result.memberid.club.idclubcodes
    data.guessed_region = (data.region_id == GENERIC_BC_REGION)

    if data.guessed_region:
        data.region_id = volunteer_result.eventid.acpregion.idclubcodes

    data.points = _calculate_points(existing_data.points if existing_data else 0,
                                    volunteer_result,
                                    multiple_distances,
                                    event_riders)
    return data

def _duplicate_event(event_list, event):
    return any(e['eventid'] == event.eventid for e in event_list)

def _find_multiples(event_list, event):
    return any(e['name'] == event.name and e['date'] == event.date \
               for e in event_list)

def _get_event_name(event_list, event):
    if event.name is None or str(event.name) == '':
        return '(Unknown)'
    if any(e['name'] == event.name for e in event_list):
        return '{} ({})'.format(event.name, str(event.date.year))
    return event.name

def _calculate_points(base_points, volunteer_result, multiple_distances, event_riders):
    points = base_points

    if volunteer_result.organizer:
        bonus_for_big_events = (_divide(event_riders, 20) * 0.5)
        points += 2.5 + bonus_for_big_events

        if _is_populaire(volunteer_result.eventid) and not multiple_distances:
            if not _small_populaire(volunteer_result.eventid, event_riders):
                points += 3.5
        if _is_fleche(volunteer_result.eventid):
            points += 1.5
    else:
        points += 1

    return points

def _small_populaire(event, event_riders):
    return (event_riders > 0 and event_riders < 50) or \
            'peace' in event.name.lower()

def _divide(num, denom):
    import six
    return num / denom if six.PY2 else num // denom
