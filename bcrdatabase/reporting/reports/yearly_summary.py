"""Calculates the yearly summary information
Determines how many riders completed an event in a given region
Also determines how many rides were completed by a rider from a given region
"""

from django.db.models import Count

import bcrdatabase.reporting.reporting_data as reporting_data
from bcrdatabase.bcrdata.models import Rideresult
from bcrdatabase.bcrdata.models import filterUnofficial, filterForFinished, filterForACPYear


class RideCount:
    """A container object for use in the summary table"""

    def __init__(self):
        self.by_region = []
        self.by_riders_region = []

    def append_regional_total(self, region_name, ride_count, member_ride_count):
        """Appends named 'rows' to the regional and rider's totals"""
        ride_count['name'] = region_name
        member_ride_count['name'] = region_name
        self.by_region.append(ride_count)
        self.by_riders_region.append(member_ride_count)


def results_filteredby_region(location):
    """Returns a list of results where the location of the ride matches the param"""
    return Rideresult.objects.filter(eventid__acpregion=location)


def results_filteredby_member(location):
    """Returns a list of results where the rider is from the param"""
    return Rideresult.objects.filter(acpregion=location)


def calculate_totals(year):
    """Perform the calculations for the relevant regions"""

    ride_count = RideCount()

    for region in reporting_data.canadian_regions():
        region_results = results_filteredby_region(region)
        count_in_region = _calculate_ridership(year, region_results)

        rider_results = results_filteredby_member(region)
        count_of_regions_members = _calculate_ridership(year, rider_results)

        ride_count.append_regional_total(region.name,
                                         count_in_region, count_of_regions_members)

    totals_for_region = _compute_totals(ride_count.by_region)
    totals_for_members = _compute_totals(ride_count.by_riders_region)
    ride_count.append_regional_total('Totals', totals_for_region, totals_for_members)

    return ride_count


def _calculate_ridership(year, raw_results):
    results_for_year = filterForACPYear(filterForFinished(raw_results), year)
    brevet_results = filterUnofficial(results_for_year).exclude(eventid__type='BCP')

    acp_brevet_distance_infos = brevet_results.exclude(eventid__type='ACPF') \
                                              .values('eventid__distance') \
                                              .annotate(Count('eventid__distance')) \
                                              .order_by('eventid__distance')
    number_of_fleche_riders = brevet_results.filter(eventid__type='ACPF').count()

    return _calculate_brevet_totals(acp_brevet_distance_infos,
                                    number_of_fleche_riders)


def _calculate_brevet_totals(acp_brevet_infos, fleche_ridership_count):
    regional_ridership_totals = {}
    total_ridership_for_region = 0

    for distance_info in acp_brevet_infos:
        distance_key = str(distance_info['eventid__distance'])
        regional_ridership_totals[distance_key] = distance_info['eventid__distance__count']
        total_ridership_for_region += distance_info['eventid__distance__count']

    regional_ridership_totals['total'] = total_ridership_for_region
    regional_ridership_totals['fleche'] = fleche_ridership_count

    return regional_ridership_totals


def _compute_totals(events_count):
    distance_totals = {}
    for rider_totals in events_count:
        if _is_name_property(rider_totals):
            continue

        for distance, counts in rider_totals.iteritems():
            if _is_name_property(counts):
                continue
            existing_count = distance_totals.get(distance, 0)
            distance_totals[distance] = existing_count + counts

    return distance_totals


def _is_name_property(prop):
    import six
    return isinstance(prop, six.string_types)
