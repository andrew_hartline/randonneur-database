"""Calculates end of year report per-rider"""

from collections import deque
from django.db.models import Sum

from bcrdatabase.bcrdata.models import Member, Rideresult
from bcrdatabase.bcrdata.models import timeSplit, minutesToTime, filterForACPYear

class ResultLine:
    """Used in printing mega-results in html"""

    def __init__(self):
        self.member_id = ""
        self.total_distance = ""
        self.name = ""
        self.location = ""
        self.date = ""
        self.ridegroup = ""
        self.b200 = {}
        self.b300 = {}
        self.b400 = {}
        self.b600 = {}
        self.b1000 = {}
        self.b1200 = {}
        self.b2000 = {}
        self.flechdist = ""
        self.flechteam = ""

    @staticmethod
    def _membertostr(member):
        if member is None:
            return ""
        else:
            return unicode(member)

    def __unicode__(self):
        return (ResultLine._membertostr(self.total_distance) + "," +
                ResultLine._membertostr(self.name) + "," +
                ResultLine._membertostr(self.location) + "," +
                ResultLine._membertostr(self.date) + "," +
                ResultLine._membertostr(self.ridegroup) + "," +
                ResultLine._membertostr(self.b200) + "," +
                ResultLine._membertostr(self.b300) + "," +
                ResultLine._membertostr(self.b400) + "," +
                ResultLine._membertostr(self.b600) + "," +
                ResultLine._membertostr(self.b1000) + "," +
                ResultLine._membertostr(self.b1200) + "," +
                ResultLine._membertostr(self.b2000) + "," +
                ResultLine._membertostr(self.flechdist) + "," +
                ResultLine._membertostr(self.flechteam))

    def __str__(self):
        return self.__unicode__()

def calculate_results(year):
    """calculates the per-rider totals for the year. EXPENSIVE"""
    year_results = filterForACPYear(Rideresult.objects.all(), year) \
                        .filter(eventid__type__in=['ACPB', 'RM', 'ACPF', 'ACPT']) \
                        .order_by('eventid__date')
    members = Member.objects.all()
    final_results = deque([])

    for member in members:
        rides_by_member = year_results.filter(memberid=member) \
                                      .filter(time__gt=1) \
                                      .order_by('eventid__acpregion',
                                                'eventid__ridegroup',
                                                'eventid__date')
        if rides_by_member.exists():
            member_result = ResultLine()
            member_result.member_id = str(member.memberid)
            event_dist_for_member = rides_by_member \
                                            .aggregate(total_distance=Sum('eventid__distance'))
            total_dist_for_member = event_dist_for_member['total_distance']
            member_result.total_distance = str(total_dist_for_member)
            member_result.name = unicode(member)
            final_results.append(member_result)

            for result in rides_by_member:
                member_row = final_results.pop()
                if (member_row.member_id != "" and member_row.location == "") \
                or (member_row.ridegroup != None and member_row.ridegroup != "" and \
                    member_row.ridegroup == unicode(result.eventid.ridegroup) and \
                    member_row.location == unicode(result.eventid.acpregion.name)):
                    pass
                else:
                    final_results.append(member_row)
                    member_row = ResultLine()

                member_row.location = unicode(result.eventid.acpregion.name)
                member_row.ridegroup = unicode(result.eventid.ridegroup)

                if member_row.date == "":
                    member_row.date = str(result.eventid.date.date())
                else:
                    member_row.date = '---'

                event_id = str(result.eventid.eventid)

                if result.eventid.type == 'ACPF':
                    member_row.flechdist = str(result.eventid.distance)
                    member_row.flechteam = result.eventid.name + " (fleche)"
                elif result.eventid.type == 'ACPT':
                    member_row.flechdist = str(result.eventid.distance)
                    member_row.flechteam = result.eventid.name + " (trace)"
                elif result.eventid.distance == 200:
                    member_row.b200[event_id] = timeSplit(minutesToTime(result.time/60/1000))
                elif result.eventid.distance == 300:
                    member_row.b300[event_id] = timeSplit(minutesToTime(result.time/60/1000))
                elif result.eventid.distance == 400:
                    member_row.b400[event_id] = timeSplit(minutesToTime(result.time/60/1000))
                elif result.eventid.distance == 600:
                    member_row.b600[event_id] = timeSplit(minutesToTime(result.time/60/1000))
                elif result.eventid.distance == 1000:
                    member_row.b1000[event_id] = timeSplit(minutesToTime(result.time/60/1000))
                elif result.eventid.distance < 2000 and result.eventid.distance > 1199:
                    member_row.b1200[event_id] = timeSplit(minutesToTime(result.time/60/1000))

                final_results.append(member_row)

    return final_results
