# Details

[https://bitbucket.org/bcrandonneurs/randonneur-bootstrap](https://bitbucket.org/bcrandonneurs/randonneur-bootstrap) is where the bootstrap theme lives.

From there, clone and compile the result if you wish to modify the local files you see here.
Then commit a new version.
