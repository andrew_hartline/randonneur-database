
$(document).ready(function () {
    BCRData.unmaskEmails(BCRData.bodyProvider);

    $('a[data-toggle=collapse]').on('click', function () {
        $(this).find('span.oi,small.oi')
            .toggleClass('oi-chevron-right')
            .toggleClass('oi-chevron-bottom');
    });
    $('[data-toggle=popover]').each(function () {
        this.dataset.content = $(this).nextAll('.popover-content')[0].innerHTML;
    }).popover();
    $('[data-toggle=tooltip]').tooltip();

    $('.form-needs-decoration').each(BCRData.turnDjangoFormsIntoBootstrapPrettyForms);
    $('a.fake').css({
        'cursor': 'default',
        'text-decoration': 'none'
    })
    .attr('href','').on('click', function(e) { e.preventDefault(); });
});

BCRData.dataTablesDOMOption = (function(DOMoptionBuilder) {
    var builder = new DOMoptionBuilder();
    with(DOMoptionBuilder.options) {
        return builder.appendRow( builder.elementOf(lengthControl),
                                  builder.elementOf(filters),
                                  loadingIndicator )
                      .append(table)
                      .appendRow( builder.elementOf(tableInfo),
                                  builder.elementOf(pageControls) )
                      .build();
    }
})(DOMoptionBuilder);

BCRData.unmaskEmails = function (listenerElementProvider) {

    listenerElementProvider().addEventListener('click', function (e) {
        if (e.target && e.target.tagName === 'A' && e.target.classList.contains('mailable')) {
            var actualEmail = unscramble(e.target.dataset.address);
            e.target.href = 'mailto:' + actualEmail;
        }
    });

    function unscramble(email) {
        return email.split('').reverse().join('');
    }
};

BCRData.turnDjangoFormsIntoBootstrapPrettyForms = function () {
    "use strict";
    
    var $thisElm = $(this);
    var $errs = $thisElm.find('ul').detach();
    $thisElm.closest('form').prepend($errs);
    $thisElm
        .find('p')
        .filter(function hasALabelElement() {
            return $(this).find('label').length > 0;
        })
        .each(function transform() {
            var $pTag = $(this);

            var $textBox = $pTag.find('input[type=text],input[type=number],input[type=password]'),
                $fileBox = $pTag.find('input[type=file]'),
                $selectBox = $pTag.find('select');

            if($fileBox.length > 0) {
                $pTag.before(makeFileBox($fileBox));
            }
            else if ($textBox.length > 0) {
                $pTag.before(makeTextBox($textBox));
            }
            else if ($selectBox.length > 0) {
                $pTag.before(makeSelectBox($selectBox));
            }
            
            replaceHelpTextWithBootstrapSmall($pTag);
        }).remove();
    $thisElm.removeClass('.form-needs-decoration');

    function makeFileBox($fileBox) {
        return function() {
            var $pTag = $(this);
            var $fileInput = $fileBox.detach();
            var $fileLabel = $pTag.find('label[for="'+$fileInput.attr('id')+'"]').detach();

            var $newFormGroup = $('<div class="form-group outlined">');
            var hasExistingFile = $pTag.find('input[type=checkbox]').length > 0;

            $newFormGroup.append($fileLabel.addClass('mr-5'));

            if(hasExistingFile) {
                var $currentValueSpan = $('<span>Currently: </span>');
                $currentValueSpan.append($pTag.find('a'));
                var $checkbox = $pTag.find('input[type=checkbox]').detach();
                $newFormGroup.append('<br/>');
                $newFormGroup.append($currentValueSpan);
                $newFormGroup.append(createCheckBoxDiv($checkbox));
                $newFormGroup.append('<span class="mr-5">Change: </span>');
            }
            $newFormGroup.append($fileInput.addClass('align-self-end'));

            return $newFormGroup;
        };

        function createCheckBoxDiv($check) {
            var $div = $('<div class="form-check">');
            var $label = $('<label class="form-check-label">Delete this file?</label>');
            $label.prepend($check.addClass('form-check-input').addClass('mr-2'));
            return $div.append($label);
        }
    }

    function makeTextBox($textBox) {
        return function() {
            var $pTag = $(this);
            var $newFormGroup = $('<div class="form-group">');
            var $input = $textBox.detach();
            $input.addClass('form-control')
                .attr('placeholder', 'Enter ' + $input.attr('name').replace('_', ' '));
            return $newFormGroup.append($pTag.find('label').detach())
                                .append($input);
        };
    }

    function makeSelectBox($selectBox) {
        return function() {
            var $pTag = $(this);
            var $newFormGroup = $('<div class="form-group">');
            $newFormGroup.append($pTag.find('label').detach());
            $selectBox.each(function(i, select) {
                var $newSelect = makeBootstrapSelect(select);
                $newFormGroup.on('click', '.dropdown-item', function () {
                    var shadowOption = select.options[this.dataset.index];
                    shadowOption.selected = true;
                    $newFormGroup.find('button').text(shadowOption.text);
                });
                $newFormGroup.append($newSelect);
            })
            return $newFormGroup;
        };
    }

    function makeBootstrapSelect(select) {
        select.style.display = 'none';
        var optionsArray = Array.apply(null, select.options);
        return $('\
        <div class="dropdown select-emulator">\
            <button class="btn dropdown-toggle" type="button" id="dropdown-'+ select.id + '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
            select.options[select.selectedIndex].text +
            '</button>\
            <div class="dropdown-menu" aria-labelledby="dropdown-'+ select.id + '">' +
            makeBootstrapOptions(optionsArray) +
        '</div>\
        </div>');
    }

    function makeBootstrapOptions(listOfItems) {
        return listOfItems.reduce(function (allHtml, option, index) {
            return allHtml + '<a class="dropdown-item" data-index="' + index + '">' + option.text + '</a>'
        }, '');
    }

    function replaceHelpTextWithBootstrapSmall($pTag) {
        $pTag.find('span.helptext')
            .replaceWith(function () {
                return '<small class="form-text text-muted">' + this.innerHTML + '</small>';
            });
    }
};