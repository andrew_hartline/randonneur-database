var DOMoptionBuilder = DOMoptionBuilder || (function() {
    var builderClass = function() {
        this._optionText = '';
        return this;
    };
    builderClass.options = { // https://datatables.net/reference/option/dom
        lengthControl: 'l',
        filters: 'f',
        table: 't',
        tableInfo: 'i',
        pageControls: 'p',
        loadingIndicator: 'r'
    };
    builderClass.prototype.append = function(value) {
        this._optionText += value;
        return this;
    };
    builderClass.prototype.elementOf = function(elementCode, elementClass) {
        return '<"[class]"[code]>'
                 .replace('[class]', elementClass || 'col-6')
                 .replace('[code]', elementCode);
    };
    builderClass.prototype.appendRow = function() {
        var argsAsArray = Array.prototype.slice.call(arguments),
            elements = jQuery.isArray(argsAsArray[0]) ? argsAsArray[0] : argsAsArray;
        this._optionText += '<"row"'+elements.join()+'>';
        return this;
    };
    builderClass.prototype.build = function() {
        return this._optionText;
    };

    return builderClass;
})();