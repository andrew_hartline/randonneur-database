from django.conf import settings
from django_ical.views import ICalFeed
from bcrdatabase.bcrdata.models import Event, getBCScheduledEvents
from datetime import datetime
from django.views.decorators.cache import cache_page

class EventFeed(ICalFeed):
    """
    A Brevet Event
    """

    product_id = '-//bcrandonneurs.bc.ca//Brevet/EN'
    timezone = 'UTC'
    file_name = 'brevets.ics'

    title = "BC Randonneurs Brevet Calendar"
    description = "Brevets Scheduled by the BC Randonneurs"

    def items(self):
        return getBCScheduledEvents().filter(date__gt=datetime(2014,1,1)).order_by('-date')

    def item_title(self, item):
        ridegroup = ""
        if item.ridegroup != "":
            ridegroup = " (" + item.ridegroup + ")" 
        return item.acpregion.name + " " + str(item.distance)  + ridegroup

    def item_description(self, item):
        return item.name 

    def item_start_datetime(self, item):
        return item.date

    def item_end_datetime(self, item):
        return item.end_datetime()

    def item_updated(self, item):
        return item.modified

    def item_location(self, item):
        return item.start

    def item_link(self, item):
        return "{0}/event/{1}".format(settings.SITE_URL, str(item.eventid))
