"""Defines views for the schedules"""

from datetime import datetime
from django.shortcuts import render
from django.views.decorators.cache import cache_page
from django.utils import timezone
from bcrdatabase.reporting.reporting_data import get_year_list
from bcrdatabase.bcrdata.models import getCanadaScheduledEvents


@cache_page(60*15)
def schedule(request, year=str(datetime.today().year)):
    """returns the single schedule page that encases all region forms"""
    region = request.GET.get("region", "all")
    schedule_list = getCanadaScheduledEvents().filter(
        date__year=year).order_by('date')
    if region != "all":
        schedule_list = schedule_list.filter(acpregion__idclubcodes=region)
    completed_event_list = schedule_list.filter(date__lte=timezone.now())
    upnext_event_list = schedule_list.filter(date__gte=timezone.now())
    return render(request, 'schedule/index.html', {'year': year,
                                                   'year_list': get_year_list(),
                                                   'completed_event_list': completed_event_list,
                                                   'upnext_event_list': upnext_event_list,
                                                   'region': region})
