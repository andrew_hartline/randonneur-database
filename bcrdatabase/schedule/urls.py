from django.conf.urls import include, url
from .eventfeed import EventFeed
from django.views.decorators.cache import cache_page

from bcrdatabase.schedule.views import *
               
urlpatterns = [url(r'^$', schedule),
               url(r'^(?P<year>\d+)/$', schedule, name='schedule'),
               url(r'^brevets.ics$', cache_page(60*60*24)(EventFeed()), name='eventfeed'),
]
