import unittest
from django.test import Client

class IndexTest(unittest.TestCase):

    def setUp(self):
        self.client = Client()

    def test_index_success(self):
        """Index page returns 200"""
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
