from bcrdatabase.bcrdata.models import Event, Clubcode, Route, Member
from django.utils import timezone

class MockClubs(object):

    id = 1

    @classmethod
    def create(cls):
        club = Clubcode.objects.create(idclubcodes=str(cls.id))
        cls.id = cls.id+1
        return club

class MockEvents(object):

    @classmethod
    def create(cls, distance=200):
        distance = distance
        region = MockClubs.create()
        route = Route.objects.create(distance=distance,acpregion=region)
        return Event.objects.create(distance=distance,date=timezone.now(),acpregion=region, routeid=route)

class MockMembers(object):

    @classmethod
    def create(cls):
        expiredate = timezone.now()
        region = MockClubs.create()
        return Member.objects.create(expiredate=expiredate, club=region)
        
