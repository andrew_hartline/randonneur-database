from django.test import TestCase
from bcrdatabase.bcrdata.models import Member, Event
from datetime import date, timedelta

class MemberTestCase(TestCase):
    def setUp(self):
        self.current = Member.objects.create(lastname="Member", firstname="Current",expiredate=date.today() + timedelta(28))
        self.past = Member.objects.create(lastname="Old", firstname="Past",expiredate=date.today() - timedelta(28))

    def test_is_current_member(self):
        """Members current member status is reported correctly"""
        self.assertTrue(self.current.is_current_member())
        self.assertFalse(self.past.is_current_member())

    def test_member_until(self):
        self.assertEqual(self.current.member_until(), self.current.expiredate.year)
        self.assertEqual(self.past.member_until(), self.past.expiredate.year)

    def test_get_brevet_results(self):
        pass

    def test_get_permanent_results(self):
        pass

    def test_total_brevet_distance(self):
        pass

    def test_total_permanent_distance(self):
        pass

    def test_get_organized_rides(self):
        pass
        
        
