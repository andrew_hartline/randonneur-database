from django.test import TestCase
from bcrdatabase.bcrdata.models import Event, Rideresult, Member
from .Mocks import MockEvents, MockMembers
from datetime import datetime, date, timedelta

class EventTestCase(TestCase):
    def test_results_homologated(self):
        """Event is homologated with one homologated result"""
        event = MockEvents.create()
        resultMember = MockMembers.create()
        resultMember2 = MockMembers.create()
        result = Rideresult.objects.create(memberid=resultMember,eventid=event, time=10, acpnumber=10)
        self.assertTrue(event.resultsHomologated())

    def test_results_not_homologated(self):
        """Event is not homologated with one unhomologated result"""
        event = MockEvents.create()
        resultMember = MockMembers.create()
        resultMember2 = MockMembers.create()

        result = Rideresult.objects.create(memberid=resultMember,eventid=event, time=10)
        self.assertFalse(event.resultsHomologated())

    def test_multiple_results_homologated(self):
        """Event is homologated with multiple homologated results"""
        event = MockEvents.create()
        resultMember = MockMembers.create()
        resultMember2 = MockMembers.create()

        result1 = Rideresult.objects.create(memberid=resultMember,eventid=event, time=10, acpnumber=10)
        result2 = Rideresult.objects.create(memberid=resultMember2,eventid=event, time=10, acpnumber=11)
        self.assertTrue(event.resultsHomologated())

    def test_multiple_results_not_homologated(self):
        """Event is not homologated with multiple unhomologated results"""
        event = MockEvents.create()
        resultMember = MockMembers.create()
        resultMember2 = MockMembers.create()

        result1 = Rideresult.objects.create(memberid=resultMember,eventid=event, time=10)
        result2 = Rideresult.objects.create(memberid=resultMember2,eventid=event, time=10)
        self.assertFalse(event.resultsHomologated())

    def test_multiple_results_some_homologated(self):
        """Event is homologated with some homologated results"""
        event = MockEvents.create()
        resultMember = MockMembers.create()
        resultMember2 = MockMembers.create()

        result1 = Rideresult.objects.create(memberid=resultMember,eventid=event, time=10, acpnumber=10)
        result2 = Rideresult.objects.create(memberid=resultMember2,eventid=event, time=10)
        self.assertTrue(event.resultsHomologated())

    def test_no_results_not_homologated(self):
        """Event with no results is not homologated"""
        event = MockEvents.create()
        self.assertFalse(event.resultsHomologated())

    def test_end_datetime_for_100km(self):
        event = MockEvents.create(distance=100)
        diff = event.end_datetime() - event.date
        self.assertEqual(diff, timedelta(hours = 13, minutes = 30))

    def test_end_datetime_for_200km(self):
        event = MockEvents.create()
        diff = event.end_datetime() - event.date
        self.assertEqual(diff, timedelta(hours = 13, minutes = 30))

    def test_end_datetime_for_300km(self):
        event = MockEvents.create(distance=300)
        diff = event.end_datetime() - event.date
        self.assertEqual(diff, timedelta(hours = 20))

    def test_end_datetime_for_400km(self):
        event = MockEvents.create(distance=400)
        diff = event.end_datetime() - event.date
        self.assertEqual(diff, timedelta(hours = 27))

    def test_end_datetime_for_600km(self):
        event = MockEvents.create(distance=600)
        diff = event.end_datetime() - event.date
        self.assertEqual(diff, timedelta(hours = 40))

    def test_end_datetime_for_1000km(self):
        event = MockEvents.create(distance=1000)
        diff = event.end_datetime() - event.date
        self.assertEqual(diff, timedelta(hours = 75))

    def test_end_datetime_for_1200km(self):
        event = MockEvents.create(distance=1200)
        diff = event.end_datetime() - event.date
        self.assertEqual(diff, timedelta(hours = 90))

    def test_end_datetime_for_1300km(self):
        event = MockEvents.create(distance=1300)
        diff = event.end_datetime() - event.date
        self.assertEqual(diff, timedelta(hours = 90))

        
