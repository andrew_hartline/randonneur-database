from django.test import TestCase
from bcrdatabase.bcrdata.models import timeAdd

class BcrDataUtilsTestCase(TestCase):
    def test_timeAdd(self):
        self.assertEqual('10', timeAdd('10', '0'))
        self.assertEqual('15', timeAdd('10', '5'))
        self.assertEqual('100', timeAdd('30', '30'))
        self.assertEqual('230', timeAdd('100', '130'))
