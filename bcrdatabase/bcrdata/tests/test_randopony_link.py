from django.test import TestCase
from .Mocks import MockEvents
from bcrdatabase.bcrdata.models import Event
from bcrdatabase.bcrdata.models import compute_randopony_link, head_randopony_link

class RandoPonyLinkTestCase(TestCase):
    def test_compute_randopony_link(self):
        event = MockEvents.create()
        expectedUrl = "http://randopony.randonneurs.bc.ca/brevets/" + "none" + "/" + str(event.distance) + "/" + event.date.strftime("%d%b%Y")
        self.assertEqual(expectedUrl, compute_randopony_link(event))

    def test_good_head_randopony_link(self):
        self.assertEqual(False, head_randopony_link("http://www.google.ca"))
        self.assertEqual(False, head_randopony_link("http://www.ashteout.ca"))
        #self.assertEqual(True, head_randopony_link("http://randopony.randonneurs.bc.ca/brevets/VI/400/09May2015"))
        self.assertEqual(False, head_randopony_link("http://randopony.randonneurs.bc.ca/brevets/VI/600/09May2015"))
