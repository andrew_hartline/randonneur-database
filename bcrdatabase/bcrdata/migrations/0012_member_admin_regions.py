# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bcrdata', '0011_auto_20150203_0644'),
    ]

    operations = [
        migrations.AddField(
            model_name='member',
            name='admin_regions',
            field=models.ManyToManyField(related_name='administrators', to='bcrdata.Clubcode'),
            preserve_default=True,
        ),
    ]
