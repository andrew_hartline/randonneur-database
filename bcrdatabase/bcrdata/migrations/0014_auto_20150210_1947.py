# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bcrdata', '0013_auto_20150205_2250'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='type',
            field=models.CharField(max_length=135, choices=[(b'ACPB', b'ACP Brevet'), (b'ACPF', b'ACP Fleche Team'), (b'ACPFE', b'ACP Fleche'), (b'ACPT', b'ACP Trace'), (b'RUSAB', b'RUSA Brevet'), (b'RM', b'Randonneurs Mondiaux'), (b'BCPop', b'BC Populaire'), (b'BCP', b'BC Permanent')]),
            preserve_default=True,
        ),
    ]
