# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bcrdata', '0005_auto_20141229_1230'),
    ]

    operations = [
        migrations.AlterField(
            model_name='member',
            name='membershipid',
            field=models.IntegerField(unique=True),
            preserve_default=True,
        ),
    ]
