# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bcrdata', '0006_auto_20141229_1234'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='member',
            name='membershipid',
        ),
    ]
