# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bcrdata', '0008_auto_20150115_0828'),
    ]

    operations = [
        migrations.CreateModel(
            name='PopulaireEvent',
            fields=[
            ],
            options={
                'verbose_name': 'Populaire',
                'proxy': True,
            },
            bases=('bcrdata.event',),
        ),
    ]
