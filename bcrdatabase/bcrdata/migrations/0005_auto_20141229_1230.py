# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

def update_membershipids(apps, schema_editor):
    # Update all membershipids
    Member = apps.get_model("bcrdata", "Member")
    for member in Member.objects.all():
        member.membershipid = member.memberid
        member.save()

class Migration(migrations.Migration):

    dependencies = [
        ('bcrdata', '0003_auto_20141216_1000'),
    ]

    operations = [
        migrations.RunPython(update_membershipids)
    ]
