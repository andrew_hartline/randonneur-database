# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bcrdata', '0007_remove_member_membershipid'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='event',
            name='remarks',
        ),
        migrations.RemoveField(
            model_name='event',
            name='status',
        ),
        migrations.RemoveField(
            model_name='route',
            name='finish',
        ),
        migrations.RemoveField(
            model_name='route',
            name='remarks',
        ),
        migrations.RemoveField(
            model_name='route',
            name='shape',
        ),
        migrations.RemoveField(
            model_name='route',
            name='start',
        ),
        migrations.AddField(
            model_name='event',
            name='start',
            field=models.CharField(default='', max_length=1024, blank=True),
            preserve_default=False,
        ),
    ]
