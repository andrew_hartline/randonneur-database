# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bcrdata', '0012_member_admin_regions'),
    ]

    operations = [
        migrations.AlterField(
            model_name='member',
            name='admin_regions',
            field=models.ManyToManyField(related_name='administrators', to='bcrdata.Clubcode', blank=True),
            preserve_default=True,
        ),
    ]
