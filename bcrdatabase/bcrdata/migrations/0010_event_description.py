# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_markdown.models


class Migration(migrations.Migration):

    dependencies = [
        ('bcrdata', '0009_populaireevent'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='description',
            field=django_markdown.models.MarkdownField(default=''),
            preserve_default=False,
        ),
    ]
