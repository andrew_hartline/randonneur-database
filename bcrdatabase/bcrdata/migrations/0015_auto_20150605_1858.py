# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

def timeToMinutes(time):
    time = str(time)

    if int(time) < 0:
        return -1

    length = len(time)

    minutes = 0
    if length > 2:
        minutes += int(time[0:length-2]) * 60
    minutes += int(time[length-2:length])
    return minutes

def results_time(apps, schema_editor):
    Results = apps.get_model("bcrdata", "Rideresult")
    for result in Results.objects.all():
        if result.time > 0:
            milliseconds = timeToMinutes(result.time) * 60 * 1000
            print result.time, " -> ", milliseconds
            result.time = milliseconds
            result.save()

class Migration(migrations.Migration):

    dependencies = [
        ('bcrdata', '0014_auto_20150210_1947'),
    ]

    operations = [
        migrations.RunPython(results_time)
    ]
