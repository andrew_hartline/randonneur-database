# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import bcrdatabase.bcrdata.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Clubcode',
            fields=[
                ('idclubcodes', models.CharField(max_length=135, serialize=False, verbose_name=b'Club Code', primary_key=True)),
                ('country', models.CharField(max_length=135, blank=True)),
                ('province', models.CharField(max_length=135, blank=True)),
                ('name', models.CharField(max_length=135, blank=True)),
                ('displayname', models.CharField(max_length=135, blank=True)),
            ],
            options={
                'ordering': ['name'],
                'db_table': 'clubcodes',
                'verbose_name': 'ACP Region',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('eventid', models.AutoField(primary_key=True, db_column=b'eventID', serialize=False, unique=True, verbose_name=b'EventID')),
                ('ridegroup', models.CharField(blank=True, max_length=135, verbose_name=b'Ride Grouping', db_column=b'rideGroup', choices=[(b'6pack', b'6-pack'), (b'BMB', b'Boston-Montreal-Boston'), (b'C1200', b'Cascade 1200'), (b'CDP', b'CDP'), (b'DW', b"Devil's Week"), (b'Diablo', b"Diablo's Triple"), (b'EdH', b'Eau de Hell'), (b'EM', b'EM'), (b'FlNW', b'Fleche NorthWest'), (b'FlP', b'Fleche Pacificque'), (b'GA', b'Granite Anvil'), (b'GRR', b'Gold Rush Randonnee'), (b'LC', b'LC'), (b'LEL', b'London-Edinburough-London'), (b'MakeUp', b'Make-up Brevets'), (b'NWCrank', b'NWCrank'), (b'PACPOP', b'Pacific Populaire'), (b'PBP', b'Paris-Brest-Paris'), (b'PHW', b'Princeton Hell Week'), (b'RGT', b'Randos Go Touring'), (b'RM', b'Rocky Mountain'), (b'Spring', b'Spring Brevet Series'), (b'Summer', b'Summer Brevet Series'), (b'SIR', b'SIR'), (b'SW', b'SW'), (b'UIE', b'Ultimate Island Explorer'), (b'WP', b'Wack Pack'), (b'VanIsle', b'VanIsle')])),
                ('distance', models.IntegerField()),
                ('date', models.DateTimeField()),
                ('status', models.CharField(max_length=135, blank=True)),
                ('remarks', models.CharField(max_length=3072, blank=True)),
                ('type', models.CharField(max_length=135, choices=[(b'ACPB', b'ACP Brevet'), (b'ACPF', b'ACP Fleche'), (b'ACPT', b'ACP Trace'), (b'RUSAB', b'RUSA Brevet'), (b'RM', b'Randonneurs Mondiaux'), (b'BCPop', b'BC Populaire'), (b'BCP', b'BC Permanent')])),
                ('name', models.CharField(max_length=135, blank=True)),
                ('rank', models.CharField(blank=True, max_length=135, verbose_name=b'Support', choices=[(b'high', b'High'), (b'medium', b'Medium'), (b'low', b'Low'), (b'none', b'None')])),
                ('url', models.CharField(max_length=768, blank=True)),
                ('controlcard', models.FileField(upload_to=b'%Y/events/control-cards/', null=True, verbose_name=b'Event Control Card', blank=True)),
                ('routesheet', models.FileField(upload_to=b'%Y/events/route-sheets/', null=True, verbose_name=b'Event Routesheet', blank=True)),
                ('gpx', models.FileField(upload_to=b'%Y/events/gpx/', null=True, verbose_name=b'Event GPX', blank=True)),
                ('acpregion', models.ForeignKey(db_column=b'acpRegion', verbose_name=b'ACP Region', to='bcrdata.Clubcode')),
            ],
            options={
                'db_table': 'events',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Eventresult',
            fields=[
                ('status', models.CharField(max_length=135, blank=True)),
                ('history', models.CharField(max_length=3072, blank=True)),
                ('trouble', models.TextField(max_length=3072, blank=True)),
                ('conditions', models.TextField(max_length=3072, blank=True)),
                ('photos', models.CharField(max_length=1024, verbose_name=b'Photos URL', blank=True)),
                ('reporturl', models.CharField(max_length=1024, verbose_name=b'Report URL', blank=True)),
                ('eventresultid', models.AutoField(serialize=False, primary_key=True)),
                ('eventid', models.OneToOneField(db_column=b'eventID', to='bcrdata.Event')),
            ],
            options={
                'db_table': 'eventresults',
                'verbose_name': 'Event Info',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Member',
            fields=[
                ('memberid', models.AutoField(primary_key=True, db_column=b'memberID', serialize=False, unique=True, verbose_name=b'MemberID')),
                ('lastname', models.CharField(max_length=135, verbose_name=b'Last Name')),
                ('firstname', models.CharField(max_length=135, verbose_name=b'First Name')),
                ('gender', models.CharField(max_length=3, choices=[(b'M', b'Male'), (b'F', b'Female')])),
                ('address', models.CharField(max_length=135, blank=True)),
                ('city', models.CharField(max_length=135, blank=True)),
                ('province', models.CharField(max_length=135, blank=True)),
                ('country', models.CharField(max_length=135, blank=True)),
                ('postalcode', models.CharField(max_length=135, verbose_name=b'Postal Code', blank=True)),
                ('dayphone', models.CharField(max_length=135, verbose_name=b'Day Phone', blank=True)),
                ('evephone', models.CharField(max_length=135, verbose_name=b'Eve Phone', blank=True)),
                ('joinyear', models.DateField(default=bcrdatabase.bcrdata.models.current_year, verbose_name=b'Join Year')),
                ('birthday', models.DateField(null=True, blank=True)),
                ('expiredate', models.DateField(verbose_name=b'Expire Date')),
                ('info_release', models.NullBooleanField(default=True, db_column=b'info-release')),
                ('bcc', models.NullBooleanField(default=False)),
                ('email', models.EmailField(max_length=135, blank=True)),
                ('emergencyname', models.CharField(max_length=135, verbose_name=b'Emergency Contact', blank=True)),
                ('emergencynumber', models.CharField(max_length=135, verbose_name=b'Emergency Contact Number', blank=True)),
                ('modified', models.DateTimeField(auto_now=True, null=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('remarks', models.TextField(max_length=6168, blank=True)),
                ('waiver', models.BooleanField(default=False)),
                ('membershipid', models.IntegerField(unique=True, null=True)),
                ('ccnid', models.IntegerField(unique=True, null=True, verbose_name=b'CCN ID')),
                ('club', models.ForeignKey(db_column=b'club', default=b'011600', to='bcrdata.Clubcode')),
                ('user', models.OneToOneField(null=True, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['lastname'],
                'db_table': 'members',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Membershipdata',
            fields=[
                ('year', models.DateField()),
                ('membershipdataid', models.AutoField(serialize=False, primary_key=True)),
                ('memberid', models.ForeignKey(to='bcrdata.Member', db_column=b'memberID')),
            ],
            options={
                'db_table': 'membershipdata',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Rideresult',
            fields=[
                ('acpnumber', models.IntegerField(null=True, verbose_name=b'Homologation Number', db_column=b'acpNumber', blank=True)),
                ('time', models.IntegerField(default=-1, blank=True)),
                ('medal', models.BooleanField(default=False)),
                ('distanceoptional', models.IntegerField(null=True, verbose_name=b'22 Hour Distance', db_column=b'distanceOptional', blank=True)),
                ('bicycleclass', models.CharField(default=b'Single', max_length=135, verbose_name=b'Bicycle Class', db_column=b'bicycleClass', choices=[(b'Single', b'Single'), (b'Tandem', b'Tandem'), (b'Fixed', b'Fixed'), (b'Recumbent', b'Recumbent'), (b'Velomobile', b'Velomobile')])),
                ('remarks', models.CharField(max_length=3072, blank=True)),
                ('volunteerpreride', models.BooleanField(default=False, verbose_name=b'Volunteer Preride', db_column=b'volunteerPreride')),
                ('official', models.BooleanField(default=False)),
                ('distanceoptional2', models.IntegerField(null=True, verbose_name=b'24 Hour Distance', db_column=b'distanceOptional2', blank=True)),
                ('rideresultid', models.AutoField(serialize=False, primary_key=True)),
                ('flechefinalcontrol', models.CharField(max_length=1024, verbose_name=b'24h Contrtol', blank=True)),
                ('acpregion', models.ForeignKey(verbose_name=b'Home Region', to='bcrdata.Clubcode')),
                ('eventid', models.ForeignKey(to='bcrdata.Event', db_column=b'eventID')),
                ('memberid', models.ForeignKey(to='bcrdata.Member', db_column=b'memberID')),
            ],
            options={
                'db_table': 'rideresults',
                'verbose_name': 'Ride Result',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Route',
            fields=[
                ('routeid', models.AutoField(serialize=False, verbose_name=b'RouteID', primary_key=True, db_column=b'routeID')),
                ('distance', models.IntegerField()),
                ('start', models.CharField(max_length=768, blank=True)),
                ('controls', models.TextField(max_length=6144, blank=True)),
                ('active', models.BooleanField()),
                ('name', models.CharField(max_length=135)),
                ('brevet', models.BooleanField()),
                ('fleche', models.BooleanField()),
                ('populaire', models.BooleanField()),
                ('remarks', models.TextField(max_length=6144, blank=True)),
                ('url', models.CharField(max_length=768, blank=True)),
                ('finish', models.CharField(max_length=768, blank=True)),
                ('lastreviewed', models.DateTimeField(null=True, verbose_name=b'Last Reviewed', blank=True)),
                ('climbing', models.IntegerField(null=True, verbose_name=b'Climbing (m)', blank=True)),
                ('shape', models.CharField(max_length=135, blank=True)),
                ('controlcard', models.FileField(upload_to=b'permanents/control-cards/', null=True, verbose_name=b'Permanent Control Card', blank=True)),
                ('routesheet', models.FileField(upload_to=b'permanents/route-sheets/', null=True, verbose_name=b'Permanent Routesheet', blank=True)),
                ('gpx', models.FileField(upload_to=b'permanents/gpx/', null=True, verbose_name=b'Permanent GPX', blank=True)),
                ('permanentid', models.IntegerField(unique=True, null=True, blank=True)),
                ('acpregion', models.ForeignKey(db_column=b'acpregion', verbose_name=b'ACP Region', to='bcrdata.Clubcode')),
                ('submitter', models.ForeignKey(db_column=b'submitter', blank=True, to='bcrdata.Member', null=True)),
            ],
            options={
                'ordering': ['distance'],
                'db_table': 'routes',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Volunteer',
            fields=[
                ('remarks', models.CharField(max_length=135, blank=True)),
                ('organizer', models.BooleanField(default=False)),
                ('volunteersid', models.AutoField(serialize=False, primary_key=True)),
                ('eventid', models.ForeignKey(to='bcrdata.Event', db_column=b'eventID')),
                ('memberid', models.ForeignKey(to='bcrdata.Member', db_column=b'memberID')),
            ],
            options={
                'db_table': 'volunteers',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='event',
            name='flechecaptain',
            field=models.ForeignKey(db_column=b'flechecaptain', verbose_name=b'Fleche Captain', to='bcrdata.Member', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='routeid',
            field=models.ForeignKey(db_column=b'routeID', verbose_name=b'Route', to='bcrdata.Route'),
            preserve_default=True,
        ),
        migrations.CreateModel(
            name='BrevetEvent',
            fields=[
            ],
            options={
                'verbose_name': 'ACP/RM Event',
                'proxy': True,
            },
            bases=('bcrdata.event',),
        ),
        migrations.CreateModel(
            name='FlecheEvent',
            fields=[
            ],
            options={
                'verbose_name': 'Fleche Team',
                'proxy': True,
            },
            bases=('bcrdata.event',),
        ),
        migrations.CreateModel(
            name='PermanentEvent',
            fields=[
            ],
            options={
                'verbose_name': 'Permanent',
                'proxy': True,
            },
            bases=('bcrdata.event',),
        ),
    ]
