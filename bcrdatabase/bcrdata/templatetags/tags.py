"""Tags for django"""

import markdown
import six

from django import template
from django.conf import settings
from django.utils.safestring import mark_safe
from bcrdatabase.bcrdata.models import timeSplit, acpTime
import bcrdatabase.reporting.sr as super_randonneurs

register = template.Library()

# tags.py


@register.filter
def markdownify(text):
    return markdown.markdown(text)


@register.filter
def srsByMember(srs, member):
    return super_randonneurs.get_by_member(srs, member)


@register.filter
def firstsrcount(srs):
    count = 0
    for sr in srs:
        # etienne has no idea why we need hasattr, but things fail if we don't have this in production
        if hasattr(sr, 'firstyear') and sr.firstyear:
            count = count + 1
    return count


@register.filter
def lookup(d, key):
    return d[key]


@register.filter
def for_csv(value):
    if isinstance(value, six.string_types) and ',' in value:
        return value.replace(',', '')
    if value is None:
        return ''
    return value


@register.simple_tag
def email_tag(email, display):
    return mark_safe('<a href="/contact-somebody" class="mailable" data-address="{}">{}</a>'.format(''.join(reversed(email)), display))


@register.simple_tag
def active(request, pattern):
    import re
    if re.search(pattern, request.path):
        return 'active'
    return ''


@register.simple_tag
def ridetype(event, pattern):
    import re
    try:
        if re.search(pattern, event.type):
            return 'active'
        return ''
    except AttributeError:
        if re.search(pattern, event['type']):
            return 'active'
        return ''


@register.simple_tag
def randopony_link(event):
    from bcrdatabase.bcrdata.util import randopony_link
    return randopony_link(event)


@register.simple_tag
def formattime(time):
    return timeSplit(str(time))


@register.simple_tag
def acptime(time):
    return acpTime(str(time))


@register.simple_tag
def formattime_days(time):
    total_minutes = time / 60 / 1000

    years = total_minutes // (60*24*365)
    days = (total_minutes - years*60*24*365) // (60*24)
    hours = (total_minutes - years*60*24*365 - days*60*24) // 60
    minutes = total_minutes - years*60*24*365 - days*60*24 - hours * 60

    ret = ""
    if years > 0:
        ret = ret + str(years) + "y "

    return ret + str(days) + "d " + str(hours) + "h " + str(minutes) + "m"


@register.simple_tag
def booleanToYes(bool):
    if bool:
        return "yes"
    else:
        return "no"


@register.simple_tag
def feet_convert(metre):
    return int(round(metre * 3.28084))


@register.simple_tag
def eventurl(event):
    if event.url is None or event.url == "":
        return settings.MAIN_SITE + "/routes/" + str(event.date.year) + "/" + str(event.acpregion.idclubcodes) + "_" + ("%02d-%02d-%02d" % (event.date.year, event.date.month, event.date.day)) + "_" + str(event.distance) + ".html"
    else:
        return event.url
