from dal import autocomplete
from django.contrib import admin
from datetime import datetime, date
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.admin import SimpleListFilter

# BCRando Models
from bcrdatabase.bcrdata.models import Member, PopulaireEvent, BrevetEvent, Eventresult, Rideresult, PermanentEvent, FlecheEvent, Clubcode, Volunteer
from bcrdatabase.bcrdata.models import Route
from .models import minutesToTime, timeToMinutes

class CurrentMemberFilter(SimpleListFilter):
    title = _('Current Members')
    parameter_name = 'currentmember'

    def lookups(self, request, model_admin):
        return (('yes', _('Current')), ('no', _('Expired')))

    def queryset(self, request, queryset):
        if self.value() == 'yes':
            return queryset.filter(expiredate__gt=date.today())
        if self.value() == 'no':
            return queryset.filter(expiredate__lt=date.today())

######################################## MEMBER ADMINISTRATION

class MemberAdminForm(forms.ModelForm):
    class Meta(object):
        model = Member
        fields = '__all__'
        widgets = {
            #'admin_regions': autocomplete.ModelSelect2Multiple(url='region-autocomplete'),
            #'club': autocomplete.ModelSelect2(url='region-autocomplete')
        }

class MemberAdmin(admin.ModelAdmin):
    """Customize presentation of Member instance in admin.
    """
    form = MemberAdminForm
    fieldsets = [
        ('Administration', {'fields': [ ('memberid', 'ccnid') , ('lastname', 'firstname', 'gender'), 'club', ('birthday', 'expiredate'), ('waiver', 'info_release', 'bcc'), 'user' ]}),
        ('Contact', {'fields': [('address', 'dayphone'), ('city', 'evephone'), ('province', 'emergencyname'), ('country', 'emergencynumber'), 'postalcode', 'email']}),
        ('Info', {'fields': [('created', 'modified'), 'remarks', 'joinyear']}),
        ('Admin Regions', {'fields': ['admin_regions']}),
    ]

    readonly_fields = ('modified' , 'created', 'joinyear', 'memberid', 'ccnid')
    list_filter = ( 'expiredate', CurrentMemberFilter)
    list_display = ( 'memberid', 'lastname', 'firstname', 'member_until', 'join_year' )
    search_fields = [ 'memberid', 'lastname', 'firstname'  ]
    ordering = ['lastname', 'firstname' ]
        
admin.site.register(Member,MemberAdmin)

######################################## RESULTS ADMINISTRATION
class VolunteerInline(admin.TabularInline):
    model = Volunteer
    extra = 1
    raw_id_fields = ["memberid"]
    fields = ['memberid', 'organizer', 'remarks']

class ResultsInputWidget(forms.TextInput):
    def render(self, name, value, **kwargs):
        if value <= 0:
            newVal = value
        else:
            newVal = minutesToTime(value/60/1000)
        return forms.TextInput.render(self,name, newVal, kwargs)

    def value_from_datadict(self, data, files, name):
        if int(data[name]) <= 0:
            return int(data[name])
        return timeToMinutes(data[name]) * 60 * 1000

class ResultsAdminForm(forms.ModelForm):
    class Meta(object):
        model = Rideresult
        widgets = {
            'time': ResultsInputWidget,
            #'memberid': autocomplete.ModelSelect2(url='member-autocomplete'),
        }
        fields = '__all__'

class ResultsInline(admin.TabularInline):
    form = ResultsAdminForm
    model = Rideresult
    extra = 1
    fields = ['memberid', 'time', 'medal', 'bicycleclass', 'volunteerpreride', 'official', 'remarks', 'acpnumber', 'acpregion']
    raw_id_fields = ["memberid"]
    readonly_fields = ['acpregion']
    ordering = ['memberid__lastname', 'memberid__firstname']

class EventResultInline(admin.StackedInline):
    model = Eventresult
    fields = ['reporturl', 'photos', 'trouble', 'conditions']

######################################## BREVET ADMINISTRATION

class BrevetAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwds):
        super(BrevetAdminForm, self).__init__(*args, **kwds)
        self.fields['routeid'].queryset = Route.objects.filter(active=1,brevet=True).order_by('name')


class BrevetAdmin(admin.ModelAdmin):
    """Customize presentation of Events
    """
    fieldsets = [ 
        (None, {'fields' : [('eventid', 'type', 'ridegroup'), ('name', 'distance', 'routeid', 'rank'), 'date', ('start', 'acpregion'), 'url', ('controlcard', 'routesheet', 'gpx'), 'description']}),
        ]
    readonly_fields = ['eventid']
    list_display = ('eventid', 'acpregion', 'distance', 'date', 'type', 'routeid')
    ordering = ['-date', 'acpregion', 'distance']
    inlines = [ResultsInline, EventResultInline, VolunteerInline]
    list_filter = ('distance', 'ridegroup', 'acpregion')
    search_fields = ('distance', 'name', 'ridegroup', 'eventid', 'date', 'type')
    save_on_top = True
    form = BrevetAdminForm

    def formfield_for_choice_field(self, db_field, request, **kwargs):
        if db_field.name == "type":
            kwargs['choices'] = (
                ("ACPB", "ACP Brevet"),
                ("RM", "Randonneurs Mondiaux"),
                ("RUSAB", "RUSA Brevet"),
                )
        return super(BrevetAdmin, self).formfield_for_choice_field(db_field, request, **kwargs)
admin.site.register(BrevetEvent,BrevetAdmin)

class PopulaireAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwds):
        super(PopulaireAdminForm, self).__init__(*args, **kwds)
        self.fields['routeid'].queryset = Route.objects.filter(active=1,populaire=True).order_by('name')
class PopulaireAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields' : [('eventid', 'type'), ('name', 'distance', 'routeid'), 'date', ('start', 'acpregion'), 'url', 'description']}),
    ]
    readonly_fields = ['eventid']
    list_display = ('eventid', 'acpregion', 'distance', 'date', 'type', 'routeid')
    ordering = ['-date', 'acpregion', 'distance']
    inlines = [ResultsInline, EventResultInline, VolunteerInline]
    list_filter = ('distance', 'ridegroup', 'acpregion')
    search_fields = ('distance', 'name', 'ridegroup', 'eventid', 'date', 'type')
    save_on_top = True
    form = PopulaireAdminForm

    def formfield_for_choice_field(self, db_field, request, **kwargs):
        if db_field.name == "type":
            kwargs['choices'] = (
                ("BCPop", "Populaire"),
            )
        return super(PopulaireAdmin, self).formfield_for_choice_field(db_field, request, **kwargs)
admin.site.register(PopulaireEvent,PopulaireAdmin)


######################################## PERMANENT ADMINISTRATION



class PermanentResultsInline(admin.TabularInline):
    form = ResultsAdminForm
    model = Rideresult
    extra = 1
    fields = ['memberid', 'time', 'bicycleclass', 'official', 'remarks']
    #raw_id_fields = ["memberid"]

class PermanentAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwds):
        super(PermanentAdminForm, self).__init__(*args, **kwds)
        self.fields['routeid'].queryset = Route.objects.exclude(permanentid__isnull=True).filter(active=1).order_by('permanentid')

class PermanentAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        obj.name = obj.routeid.name
        obj.distance = obj.routeid.distance
        obj.acpregion = obj.routeid.acpregion
        obj.save()

    fieldsets = [ 
        (None, {'fields' : [('eventid', 'type'), ('name', 'distance', 'routeid'), 'date', 'acpregion', 'url']}),
        ]
    ordering = ['-date', 'eventid']
    readonly_fields = ['eventid', 'type', 'name', 'distance', 'acpregion']
    #prepopulated_fields = {"name": ("routeid__name",)}
    list_display = ( 'eventid', 'contains_unofficial', 'date', 'name', 'distance', 'routeid', 'acpregion' )
    inlines = [PermanentResultsInline]
    form = PermanentAdminForm

admin.site.register(PermanentEvent, PermanentAdmin)

######################################## FLECHE ADMINISTRATION

class FlecheResultInline(admin.TabularInline):
    model = Rideresult
    form = ResultsAdminForm
    extra = 1
    fields = ['memberid', 'time', 'medal', 'bicycleclass', 'distanceoptional', 'distanceoptional2', 'flechefinalcontrol', 'official', 'remarks', 'acpnumber' ]
    ordering = ['memberid__lastname', 'memberid__firstname']
    #raw_id_fields = ["memberid"]
    #readonly_fields = ['acpnumber']

class FlecheAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwds):
        super(FlecheAdminForm, self).__init__(*args, **kwds)
        self.fields['routeid'].queryset = Route.objects.filter(active=1,fleche=True).order_by('name')


class FlecheAdmin(admin.ModelAdmin):
    fieldsets = [ 
        (None, {'fields' : [('eventid', 'type', 'ridegroup'), ('name', 'distance', 'routeid'), 'flechecaptain', 'date', ('start', 'acpregion')]}),
        ]
    readonly_fields = ['eventid']
    list_display = ('type', 'name', 'distance', 'flechecaptain', 'routeid')
    inlines = [FlecheResultInline, EventResultInline]
    form = FlecheAdminForm
admin.site.register(FlecheEvent, FlecheAdmin)



######################################## CLUB CODE ADMINISTRATION
class ClubcodeAdmin(admin.ModelAdmin):
    search_fields = [ 'name', 'idclubcodes', 'country', 'province' ]
    list_display = ('idclubcodes', 'name', 'country', 'province')
admin.site.register(Clubcode, ClubcodeAdmin)

######################################## ROUTE ADMINISTRATION
class RouteAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields' : [('routeid', 'name', 'distance',  'climbing', 'active')]}),
        (None, {'fields' : [ 'acpregion', 'controls', 'lastreviewed']}),
        (None, {'fields' : [('brevet', 'fleche', 'populaire', 'permanentid')]}),
        (None, {'fields' : [('controlcard', 'routesheet', 'gpx')]}),
        (None, {'fields' : [('submitter', 'url')]}),
    ]
    readonly_fields = ['routeid']
    ordering = ['routeid']
    search_fields = ['controls', 'routeid', 'distance', 'name' ]
    list_display = ('routeid', 'permanentid', 'acpregion', 'controls', 'distance', 'name', 'climbing', 'brevet', 'active')
    list_filter = ['brevet']
    
admin.site.register(Route, RouteAdmin)
