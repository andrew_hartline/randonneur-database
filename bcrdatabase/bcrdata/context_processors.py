from models import Clubcode
from django.conf import settings

def site_url(httpRequest):
    return { 'SITE_URL': settings.SITE_URL }

def region_list(httpRequest):
    region_list = Clubcode.objects.filter(country='CA').exclude(idclubcodes='011600').order_by('name')
    return { 'region_list': region_list }
