from django.conf.urls import include, url
from bcrdatabase.bcrdata.views import index, event, member, route, browse, auth_error, browseMembers, browseRandonnees, browseRoutes, browsePermanents, browseFleches, MemberAutocomplete, ClubcodeAutocomplete

urlpatterns = [# Index
    url(r'^$', index, name='home'),
    
    # Urls for viewing database objects
    url(r'^event/(?P<event_id>\d+)/$', event,name='event'),
    url(r'^member/(?P<member_id>\d+)/$', member,name='member'),
    url(r'^route/(?P<route_id>\d+)/$', route,name='route'),
    
    # Browsing views
    url(r'^browse$', browse),
    url(r'^browse/members$', browseMembers,name='members'),
    url(r'^browse/randonnees$', browseRandonnees,name='randonnees'),
    url(r'^browse/routes$', browseRoutes,name='routes'),
    url(r'^browse/permanents$', browsePermanents,name='permanents'),
    url(r'^browse/fleches$', browseFleches,name='fleches'),
    
    url(r'^autocomplete/member/$', MemberAutocomplete.as_view(), name='member-autocomplete'),
    url(r'^autocomplete/region/$', ClubcodeAutocomplete.as_view(), name='region-autocomplete'),

    url(r'^auth_error/?$', auth_error),
]    
