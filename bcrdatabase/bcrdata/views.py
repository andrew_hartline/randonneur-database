from django.conf import settings
from django.shortcuts import get_object_or_404, redirect, render
from django import forms
from django.conf import settings
from django.views.decorators import csrf
from django.template import RequestContext
from django.db.models import Q

from dal import autocomplete

from datetime import datetime, date

from .models import Member, Route, Rideresult, Volunteer, Clubcode
from .models import Event, Eventresult, BrevetEvent, PermanentEvent, FlecheEvent
from .models import filterUnofficial, filterForFinished, filterUnknown, filterInBC
from .models import filterEventsInBC, filterCompleted, filterUnknown
from .models import getRecentPermanents, getUpcomingEvents


class MemberEventsForm(forms.Form):
    year = forms.IntegerField(required=False)

def brevet(request, event_id):
    event = get_object_or_404(Event, pk=event_id, type='ACPB')
    return event(request, event_id)

def permanent(request, event_id):
    event = get_object_or_404(Event, pk=event_id, type='BCP')
    return event(request, event_id)

def event(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    results = Rideresult.objects.filter(eventid=event_id).order_by('memberid__lastname', 'memberid__firstname')
    volunteers = Volunteer.objects.filter(eventid=event_id).order_by('memberid__lastname', 'memberid__firstname')
    try:
        eventresult = Eventresult.objects.get(eventid=event_id)
    except:
        eventresult = None

    if event.isFleche():
        template = "bcrdata/fleche.html"
    else:
        template = "bcrdata/event.html"

    return render(request, template, {'event': event,
                                      'results' : results,
                                      'volunteers' : volunteers,
                                      'eventresult': eventresult})

def member(request, member_id):
    form = MemberEventsForm()

    year = None
    if request.method == 'POST':
        form = MemberEventsForm(request.POST)
        if form.is_valid():
            year = form.cleaned_data['year']

    member = get_object_or_404(Member, pk=member_id)
    
    if year != None:
        brevetResults = member.getBrevetResults().filter(eventid__date__year=year).order_by('-eventid__date')
        permResults = member.getPermanentResults().filter(eventid__date__year=year).order_by('-eventid__date')
    else:
        brevetResults = member.getBrevetResults().order_by('-eventid__date')
        permResults = member.getPermanentResults().order_by('-eventid__date')
    brevetResults = filterUnofficial(brevetResults)
    totalBrevetDistance = member.totalBrevetDistance()
    totalPermanentDistance = member.totalPermanentDistance()
    totalRandonnees = brevetResults.count()
    totalPermanents = permResults.count()
    designedRoutes = Route.objects.filter(submitter=member_id)
    volunteers = Volunteer.objects.filter(memberid=member_id).order_by('eventid__date')
    
    return render(request, 'bcrdata/member.html', {'form' : form,
                                                   'year' : year,
                                                   'member' : member,
                                                   'brevetResults' : brevetResults, 
                                                   'permResults' : permResults,
                                                   'brevetDist' : totalBrevetDistance,
                                                   'brevetTotal': totalRandonnees,
                                                   'permDist' : totalPermanentDistance,
                                                   'permTotal': totalPermanents,
                                                   'volunteers' : volunteers,
                                                   'designedRoutes': designedRoutes,
                                                   'event': {'type': ''}})

def route(request, route_id):
    route = get_object_or_404(Route, pk=route_id)
    events = Event.objects.filter(routeid=route_id).order_by('-date')
    results = filterUnknown(filterForFinished(Rideresult.objects.filter(eventid__routeid=route_id).order_by('time')))[:15]
    if len(events) > 0:
        recent_event = events[0]
    else:
        recent_event = None

    return render(request, 'bcrdata/route.html', {'event' : {'type': ""},
                                                  'route' : route,
                                                  'recent_event' : recent_event,
                                                  'events' : events,
                                                  'results' : results})

def auth_error(request):
    context = { 'reason': "Did you forget to ask for permissions to do that?" }
    if request.user.is_anonymous():
        return redirect(settings.LOGIN_URL)
    elif not hasattr(request.user, 'member'):
        context['reason'] = "There's no member associated with your user!"
    elif not request.user.member.is_current_member():
        context['reason'] = "Your membership has expired! Renew!"

    return render(request, 'bcrdata/auth_error.html', context)

def browse(request):
    return render(request,
                  'bcrdata/browse.html',
                  {'event': {'type': ''}})

def browseMembers(request):
    search_results = filterUnknown(Member.objects.order_by('lastname', 'firstname'))

    return render(request, 'bcrdata/members.html',
                  {'member_results': search_results,
                   'event': {'type': ''}})

def browseFleches(request):
    search_results = filterEventsInBC(FlecheEvent.objects.order_by('-date', 'type', 'name'))

    return render(request, 'bcrdata/fleches.html',
                  {'event' : {'type': 'ACPF'},
                   'event_results': search_results})

def browseRandonnees(request):
    search_results = filterCompleted(filterEventsInBC(BrevetEvent.objects.order_by('-date')))

    return render(request, 'bcrdata/events.html',
                  {'event' : {'type': 'ACPB'},
                   'event_results': search_results})

def browseRoutes(request):
    search_results = Route.objects.exclude(routeid=100).order_by('routeid')

    return render(request, 'bcrdata/routes.html',
                  {'event': {'type': ''},
                   'route_results': search_results})

def browsePermanents(request):
    #search_results = Rideresult.objects.filter(eventid__type='BCP').order_by('-eventid__date','eventid__eventid', 'memberid__lastname','memberid__firstname')
    search_results = PermanentEvent.objects.order_by('-date','eventid')

    return render(request, 'bcrdata/permanents.html',
                  {'event': {'type': 'BCP' },
                   'event_results': search_results})

def index(request):
    if settings.DEBUG:
        import sys
        print >>sys.stderr, (">>>>>>>>>> IP Address of request: " + request.META['REMOTE_ADDR'])
    return render(request, 'bcrdata/index.html',
                  {'event': { 'type' : "" },
                   'recent_permanents' : getRecentPermanents(),
                   'upcoming_events' : getUpcomingEvents(),
                })

class MemberAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return Member.objects.none()
        qs = Member.objects.all()

        if self.q:
            qs = qs.filter(Q(firstname__icontains=self.q) | Q(lastname__icontains=self.q))

        return qs

class ClubcodeAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return Clubcode.objcets.none()
        qs = Clubcode.objects.all()

        if self.q:
            qs = qs.filter(name__icontains=self.q)

        return qs
