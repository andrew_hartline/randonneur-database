# -*- coding: utf-8 -*-

import logging
from datetime import datetime, timedelta
from string import split

import requests
from django.contrib.auth.models import User
from django.contrib.auth.signals import user_logged_in
from django.core.cache import cache
from django.db import models
from django.db.models import Max, Sum
from django.db.models.signals import post_save
from django.utils import timezone
from django_markdown.models import MarkdownField

"""Core data models for the app"""




logger = logging.getLogger(__name__)

def randopony_link(event):
    randopony_link = compute_randopony_link(event)
    active_randopony_link = cache.get(randopony_link)

    if active_randopony_link is None:
        active_randopony_link = head_randopony_link(randopony_link)
        cache.set(randopony_link, active_randopony_link, 60*60)

    if (active_randopony_link):
        return randopony_link
    else:
        return None
    
def compute_randopony_link(event):
    randopony_base = "http://randopony.randonneurs.bc.ca/brevets"
    region_abbrev = event.acpregion.get_abbrev_name()
    distance = str(event.distance)
    date = event.date.strftime("%d%b%Y")
    return randopony_base + "/" + region_abbrev + "/" + distance + "/" + date
    

def head_randopony_link(url):
    try:
        response = requests.get(url)
        if response.status_code >= 200 and response.status_code < 300:
            return "Route" in response.text and "Start" in response.text and "Start Location" in response.text
        else:
            return False
        
    except:
        return False

def getBCScheduledEvents():
    return Event.objects.filter(type__in=Event.BC_EVENT_TYPES).filter(acpregion__country='CA').filter(acpregion__province='BC')


def getCanadaScheduledEvents():
    return Event.objects.filter(type__in=Event.BC_EVENT_TYPES).filter(acpregion__country='CA')

def currentMembers():
    return Member.objects.filter(expiredate__gte=timezone.now())

def filterForBrevets(results):
    return results.filter(eventid__type='ACPB')

def filterForACPYear(results, year):
    prev_year = timezone.make_aware(datetime(year-1,11,1))
    this_year = timezone.make_aware(datetime(year,11,1))
    return results.filter(eventid__date__gte=prev_year,eventid__date__lt=this_year)

def filterForFinished(results):
    return results.exclude(time__lte=-3).exclude(time=-1)

def filterUnknown(results):
    return results.exclude(time=-2)

def filterUnofficial(results):
    return results.exclude(official=0)

def filterInBC(results):
    return results.filter(eventid__acpregion__country='CA',eventid__acpregion__province='BC')

def filterEventsInBC(events):
    return events.filter(acpregion__country='CA',acpregion__province='BC')

def filterCompleted(events):
    return events.filter(date__lte=timezone.now())

def filterUnknown(members):
    return members.exclude(memberid=1770)

def timeSplit(t):
    if t == -1 or t == "-1":
        return "DNF"
    if t == -2 or t == "-2":
        return "??:??"
    if t == -3 or t == "-3":
        return "DQ"
    if t == -4 or t == "-4":
        return "DNS"
    if t == -5 or t == "-5":
        return "HD"
    t = str(t)
    length = len(t)
    ret = t[0:length-2] + ":" + t[length-2:length]
    return ret

def acpTime(t):
    if t == -1 or t == "-1":
        return "DNF"
    if t == -2 or t == "-2":
        return "??:??"
    if t == -3 or t == "-3":
        return "DQ"
    if t == -4 or t == "-4":
        return "DNS"
    if t == -5 or t == "-5":
        return "HD"
    t = str(t)
    length = len(t)
    ret = t[0:length-2] + " h " + t[length-2:length]
    return ret

def minutesToTime(minutes):
    minuteField = minutes % 60

    if (minuteField < 10):
        minuteField = "0" + str(minuteField)
    else:
        minuteField = str(minuteField)

    hourField = minutes//60

    if hourField > 0:
        return str(hourField) + minuteField
    else:
        return minuteField

def timeToMinutes(time):
    time = str(time)

    if int(time) < 0:
        return -1

    length = len(time)

    minutes = 0
    if length > 2:
        minutes += int(time[0:length-2]) * 60
    minutes += int(time[length-2:length])
    return minutes

def timeAdd(x, y):
    return minutesToTime(timeToMinutes(x) + timeToMinutes(y))

def getRecentPermanents():
    return Rideresult.objects.filter(eventid__type='BCP', time__gt=0).order_by('-eventid__date')[:10]

def getUpcomingEvents():
    today = timezone.now()
    fourweeks = today + timedelta(28)

    return Event.objects.filter(date__gte=today,date__lte=fourweeks).order_by('date')

class Clubcode(models.Model):
    idclubcodes = models.CharField(max_length=135, primary_key=True, verbose_name='Club Code')
    country = models.CharField(max_length=135, blank=True)
    province = models.CharField(max_length=135, blank=True)
    name = models.CharField(max_length=135, blank=True)
    displayname = models.CharField(max_length=135, blank=True)
    
    class Meta:
        db_table = u'clubcodes'
        ordering = ['name']
        verbose_name = "ACP Region"

    def to_dict(self):
        return { 'id': self.idclubcodes,
                 'country': self.country,
                 'province': self.province,
                 'name': self.name,
                 'displayname': self.displayname 
                 }

    def get_abbrev_name(self):
        if (self.idclubcodes == "011602"):
            return "LM"
        elif (self.idclubcodes == "011611"):
            return "VI"
        elif (self.idclubcodes == "011621"):
            return "SI"
        elif (self.idclubcodes == "011641"):
            return "PR"
        else:
            return "none"

    def __unicode__(self):
        return self.name

def current_year():
    return datetime(timezone.now().year,1,1)
        
class Member(models.Model):
    GENDER_CHOICE = (("M", "Male"), ("F", "Female"), ("X", "X"))

    memberid = models.AutoField(primary_key=True, unique=True, db_column='memberID', verbose_name='MemberID') # Field name made lowercase.
    lastname = models.CharField(max_length=135, verbose_name='Last Name')
    firstname = models.CharField(max_length=135, verbose_name='First Name')
    gender = models.CharField(max_length=3,choices=GENDER_CHOICE)
    address = models.CharField(max_length=135, blank=True)
    city = models.CharField(max_length=135, blank=True)
    province = models.CharField(max_length=135, blank=True)
    country = models.CharField(max_length=135, blank=True)
    postalcode = models.CharField(max_length=135, blank=True, verbose_name='Postal Code')
    dayphone = models.CharField('Day Phone', max_length=135, blank=True)
    evephone = models.CharField('Eve Phone', max_length=135, blank=True)
    club = models.ForeignKey(Clubcode, db_column='club', default='011600')
    admin_regions = models.ManyToManyField(Clubcode, related_name='administrators', blank=True)
    joinyear = models.DateField(default=current_year, verbose_name='Join Year')
    birthday = models.DateField(null=True, blank=True)
    expiredate = models.DateField('Expire Date')
    info_release = models.NullBooleanField(null=True, db_column='info-release', default=True, blank=True) # Field renamed to remove dashes. Field name made lowercase.
    bcc = models.NullBooleanField(null=True, blank=True, default=False)
    email = models.EmailField(max_length=135, blank=True)
    emergencyname = models.CharField('Emergency Contact', max_length=135, blank=True)
    emergencynumber = models.CharField('Emergency Contact Number', max_length=135, blank=True)
    modified = models.DateTimeField(auto_now=True, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, blank=True)
    remarks = models.TextField(max_length=6168, blank=True)
    waiver = models.BooleanField(blank=True, default=False)
    ccnid = models.IntegerField('CCN ID', null=True,unique=True)

    user = models.OneToOneField(User,blank=True,null=True,on_delete=models.SET_NULL)

    def to_dict(self):
        return { 'id' : self.memberid,
                 'lastname' : self.lastname,
                 'firstname' : self.firstname,
                 'gender' : self.gender,
                 'club' : self.club.idclubcodes,
                 'joinyear' : self.joinyear.year,
                 'expiredate' : self.expiredate.year,
                 'brevet_distance' : self.totalBrevetDistance(),
                 'brevet_total' : len(self.getBrevetResults()),
                 'perm_dist' : self.totalPermanentDistance(),
                 'perm_total' : len(self.getPermanentResults()),
                 }

    class Meta:
        db_table = u'members'
        ordering = ['lastname']

    def __unicode__(self):
        return self.lastname + ", " + self.firstname

    def save(self, *args, **kwargs):
        super(Member, self).save(*args, **kwargs)
        for year in range(timezone.now().year, self.expiredate.year):
            count = Membershipdata.objects.filter(memberid__memberid=self.memberid).filter(year__year=year).count()
            if count == 0:
                data = Membershipdata(memberid=self, year=datetime(year,1,1))
                data.save()

    def is_regional_admin(self):
        return self.admin_regions.exists()

    def is_current_member(self):
        return timezone.localdate() < self.expiredate
    is_current_member.short_description = "Current Member?"

    def member_until(self):
        return self.expiredate.year
    member_until.short_description = "Member Until (Incl)"

    def _get_join_year(self):
        return self.joinyear.year
    join_year = property(_get_join_year)

    def _fullname(self):
        return self.firstname + " " + self.lastname
    fullname = property(_fullname)

    def getBrevetResults(self):
        return Rideresult.objects.filter(memberid=self.memberid).exclude(eventid__type='BCP')

    def getPermanentResults(self):
        return Rideresult.objects.filter(memberid=self.memberid).filter(eventid__type='BCP')

    def totalBrevetDistance(self):
        return filterUnofficial(self.getBrevetResults()).exclude(time__lte=-3).exclude(time=-1).aggregate(Sum('eventid__distance'))['eventid__distance__sum']
    def totalPermanentDistance(self):
        return filterUnofficial(self.getPermanentResults()).exclude(time__lte=-3).exclude(time=-1).aggregate(Sum('eventid__distance'))['eventid__distance__sum']

    def getOrganizedRides(self):
        organized = Volunteer.objects.select_related().filter(memberid=self,organizer=True)
        def toEvent(volunteer):
            return volunteer.eventid
        return map(toEvent, organized)
        

class Route(models.Model):
    routeid = models.AutoField(primary_key=True, db_column='routeID', verbose_name='RouteID') # Field name made lowercase.
    distance = models.IntegerField()
    acpregion = models.ForeignKey(Clubcode, verbose_name='ACP Region', db_column='acpregion') # Field name made lowercase.
    controls = models.TextField(max_length=6144, blank=True)
    submitter = models.ForeignKey(Member, null=True, db_column='submitter', blank=True)
    active = models.BooleanField(default=False)
    name = models.CharField(max_length=135)
    brevet = models.BooleanField(default=False)
    fleche = models.BooleanField(default=False)
    populaire = models.BooleanField(default=False)
    url = models.CharField(max_length=768, blank=True)
    lastreviewed = models.DateTimeField(null=True, blank=True, verbose_name='Last Reviewed')
    climbing = models.IntegerField(null=True, blank=True, verbose_name='Climbing (m)')
    controlcard = models.FileField(upload_to='permanents/control-cards/', verbose_name='Permanent Control Card', null=True,blank=True)
    routesheet = models.FileField(upload_to='permanents/route-sheets/', verbose_name='Permanent Routesheet',null=True,blank=True)
    gpx = models.FileField(upload_to='permanents/gpx/', verbose_name='Permanent GPX',null=True,blank=True)
    permanentid = models.IntegerField(null=True, blank=True, unique=True)
    class Meta:
        db_table = u'routes'
        ordering = ['distance']

    def isPermanent(self):
        return self.permanentid != None
    isPermanent.short_description = "Permanent Route"

    def to_dict(self):
        return { 'id' : self.routeid,
                 'url': self.url,
                 'name': self.name,
                 'distance': self.distance,
                 'acpregion': self.acpregion.to_dict(),
                 'controls': self.controls,
                 'permanentid': self.permanentid,
                 'active': self.active,
                 'routesheet': self.routesheet.url if self.routesheet else None,
                 'controlcard': self.controlcard.url if self.controlcard else None,
                 'gpx': self.gpx.url if self.gpx else None}

    def __unicode__(self):
        if self.routeid == 100:
            return "Unknown"
        else:
            return self.name + " (" + str(self.distance) + ")"

class Event(models.Model):
    BC_EVENT_TYPES = ['ACPB', 'BCPop', 'RM', 'ACPFE']
    ACP_EVENT_TYPES = ['ACPB', 'RM' ]
    TYPE_CHOICES = (('ACPB', 'ACP Brevet'),
                    ('ACPF', 'ACP Fleche Team'),
                    ('ACPFE', 'ACP Fleche'),
                    ('ACPT', 'ACP Trace'),
                    ('RUSAB', 'RUSA Brevet'),
                    ('RM', 'Randonneurs Mondiaux'),
                    ('BCPop', 'BC Populaire'),
                    ('BCP', 'BC Permanent'),
                    )
    RIDEGROUP_CHOICES = (('6pack','6-pack'),
                         ('BMB', 'Boston-Montreal-Boston'),
                         ('C1200', 'Cascade 1200'),
                         ('CDP','CDP'),
                         ('DW',"Devil's Week"),
                         ('Diablo', "Diablo's Triple"),
                         ('EdH','Eau de Hell'),
                         ('EM','EM'),
                         ('FlNW','Flèche NorthWest'),
                         ('FlP','Flèche Pacificque'),
                         ('FlO','Flèche Ontario'),
                         ('FlM','Flèche Montréal'),
                         ('GA','Granite Anvil'),
                         ('GRR','Gold Rush Randonnee'),
                         ('LC','LC'),
                         ('LEL','London-Edinburough-London'),
                         ('MakeUp', 'Make-up Brevets'),
                         ('NWCrank','NWCrank'),
                         ('PACPOP','Pacific Populaire'),
                         ('PacTour', 'PacTour'),
                         ('5Point', '5 Point Tune-Up'),
                         ('PBP','Paris-Brest-Paris'),
                         ('PHW','Princeton Hell Week'),
                         ('RGT','Randos Go Touring'),
                         ('RM','Rocky Mountain'),
                         ('Spring', 'Spring Brevet Series'),
                         ('Summer', 'Summer Brevet Series'),
                         ('SIR','SIR'),
                         ('SW','SW'),
                         ('UIE','Ultimate Island Explorer'),
                         ('WP', 'Wack Pack'),
                         ('VanIsle','VanIsle'),
                         )

    RANK_CHOICES = (('high', 'High'),
                    ('medium', 'Medium'),
                    ('low', 'Low'),
                    ('none', 'None'))

    eventid = models.AutoField(primary_key=True, unique=True, db_column='eventID', verbose_name='EventID') 
    ridegroup = models.CharField("Ride Grouping", max_length=135, db_column='rideGroup', choices=RIDEGROUP_CHOICES, blank=True) 
    distance = models.IntegerField()
    date = models.DateTimeField()
    description = MarkdownField(null=True)
    acpregion = models.ForeignKey(Clubcode, verbose_name='ACP Region', db_column='acpRegion') 
    routeid = models.ForeignKey(Route, verbose_name='Route', db_column='routeID') 
    type = models.CharField(max_length=135, choices=TYPE_CHOICES)
    name = models.CharField(max_length=135, blank=True)
    rank = models.CharField('Support', max_length=135, blank=True, choices=RANK_CHOICES)
    start = models.CharField(max_length=1024, blank=True)
    url = models.CharField(max_length=768, blank=True)
    controlcard = models.FileField(upload_to='%Y/events/control-cards/', verbose_name='Event Control Card',null=True,blank=True)
    routesheet = models.FileField(upload_to='%Y/events/route-sheets/', verbose_name='Event Routesheet',null=True,blank=True)
    gpx = models.FileField(upload_to='%Y/events/gpx/', verbose_name='Event GPX',null=True,blank=True)
    flechecaptain = models.ForeignKey(Member, db_column='flechecaptain', verbose_name='Fleche Captain', null=True)
    modified = models.DateTimeField(auto_now=True, blank=True, null=True)

    class Meta:
        db_table = u'events'

    @staticmethod
    def filterACPBrevetsInYear(year, events=None):
        events_query=events if events else Event.objects
        prev_year = timezone.make_aware(datetime(year-1,11,1))
        this_year = timezone.make_aware(datetime(year,11,1))
        return events_query.filter(type__in=Event.ACP_EVENT_TYPES) \
                           .filter(date__gte=prev_year,date__lt=this_year)

    def isInBC(self):
        return self.acpregion.country == 'CA' and self.acpregion.province == 'BC'

    def isBrevet(self):
        return self.type == 'ACPB'

    def isPermanent(self):
        return self.type == 'BCP'

    def isFleche(self):
        return self.type == 'ACPF' or self.type == 'ACPT'

    def contains_unofficial(self):
        if Rideresult.objects.filter(eventid=self.eventid).filter(official=0).exists():
            return "Yes"
        else:
            return ""
    contains_unofficial.short_description = "Requires verification?"

    def _is_in_past(self):
        return timezone.now() > self.date
    is_in_past = property(_is_in_past)

    def getRandoPonyLink(self):
        if not self.is_in_past:
            return randopony_link(self)
        return None

    def getResults(self):
        return Rideresult.objects.filter(eventid=self.eventid)

    def getVolunteers(self):
        return Volunteer.objects.filter(eventid=self.eventid)

    def hasOrganizer(self):
        return self.volunteer_set.filter(organizer=True).exists()

    def hasEventResult(self):
        return Eventresult.objects.filter(eventid=self.eventid).exists()

    def getEventResult(self):
        return Eventresult.objects.get(eventid=self.eventid)

    def resultsHomologated(self):
        return self.rideresult_set.filter(acpnumber__isnull=False).filter(time__gte=0).exists()

    def status(self):
        if not self.is_in_past and not self.hasOrganizer():
            return "Reqires organizer"
        elif not self.is_in_past:
            return "Not yet run"
        elif self.hasEventResult() and self.contains_unofficial():
            return "Waiting for result verification"
        elif not self.hasEventResult():
            return "Waiting for results"
        elif not self.resultsHomologated():
            return "Waiting for homologation"
        else:
            return "Homologated"

    def to_dict(self, expand=True):
         event =  { 'id' : self.eventid,
                    'name' : self.name,
                    'distance' : self.distance,
                    'date' : str(self.date.year) + "-" + str(self.date.month) + "-" + str(self.date.day),
                    'datetime' : str(self.date),
                    'acpregion' : self.acpregion.to_dict(),
                    'route' : self.routeid.to_dict(),
                    'type' : self.type,
                    'url' : self.url,
                    'ridegroup' : next((x[1] for x in Event.RIDEGROUP_CHOICES if x[0] == self.ridegroup), ""),
                    'start': self.start,
                    'controlcard' : self.controlcard.url if self.controlcard else None,
                    'routesheet' : self.routesheet.url if self.routesheet else None,
                    'gpx' : self.gpx.url if self.gpx else None,
         }
         if expand:
             event['results'] = map(lambda r: r.to_dict(), self.getResults().order_by('time', 'memberid__lastname', 'memberid__firstname'))
             event['volunteers'] = map(lambda v: v.to_dict(), self.getVolunteers())
             event['eventresult'] = self.getEventResult().to_dict() if self.hasEventResult() else None,
         return event

    def end_datetime(self):
        distance = self.distance
        start_time = self.date

        # Less than 300 gets the time for 200km brevets.
        # More then 1000 gets the time for 1000km brevets.
        if distance < 300:
            end_time = start_time + timedelta(hours = 13, minutes = 30)
        elif distance >= 300 and distance < 400:
            end_time = start_time + timedelta(hours = 20)
        elif distance >= 400 and distance < 600:
            end_time = start_time + timedelta(hours = 27)
        elif distance >= 600 and distance < 1000:
            end_time = start_time + timedelta(hours = 40)
        elif distance >= 1000 and distance < 1200:
            end_time = start_time + timedelta(hours = 75)
        else:
            end_time = start_time + timedelta(hours = 90)

        return end_time

    def date_and_name(self):
        return str(self.date.year)+ "-" + str(self.date.month) + "-" + str(self.date.day) + ": " + unicode(self.name) if self.name else "<unknown event>"

    def __unicode__(self):
        return self.type + " " + str(self.distance) + "km " + str(self.date.year)+ "-" + str(self.date.month) + "-" + str(self.date.day) + ": " + unicode(self.name)

class PermanentEventManager(models.Manager):
    def get_queryset(self):
        return super(PermanentEventManager,self).get_queryset().filter(type='BCP')
class PermanentEvent(Event):
    class Meta:
        proxy = True
        verbose_name = "Permanent"
    objects = PermanentEventManager()

    def to_dict(self):
        return { 'id' : self.eventid,
                 'distance': self.distance,
                 'date': str(self.date.year) + "-" + str(self.date.month) + "-" + str(self.date.day),
                 'acpregion': self.acpregion.to_dict(),
                 'route' : self.routeid.to_dict(),
                 'results': map(lambda r : r.to_dict(), Rideresult.objects.filter(eventid__eventid=self.eventid))
                 }

    def save(self, *args, **kwargs):
        self.type = 'BCP'
        super(Event, self).save(*args, **kwargs)

class BrevetEventManager(models.Manager):
    def get_queryset(self):
        return super(BrevetEventManager,self).get_queryset().filter(type__in=['ACPB', 'RM'])
class BrevetEvent(Event):
    class Meta:
        proxy = True
        verbose_name = "ACP/RM Event"
    objects = BrevetEventManager()

    def save(self, *args, **kwargs):
        if self.type == "" or self.type == None:
            self.type = 'ACPB'
        super(Event, self).save(*args, **kwargs)

class PopulaireEventManager(models.Manager):
    def get_queryset(self):
        return super(PopulaireEventManager,self).get_queryset().filter(type__in=['BCPop'])
class PopulaireEvent(Event):
    class Meta:
        proxy = True
        verbose_name = "Populaire"
    objects = PopulaireEventManager()

    def save(self, *args, **kwargs):
        if self.type == "" or self.type == None:
            self.type = 'BCPop'
        super(Event, self).save(*args, **kwargs)

class FlecheEventManager(models.Manager):
    def get_queryset(self):
        return super(FlecheEventManager,self).get_queryset().filter(type__in=['ACPF', 'ACPT', 'ACPFE'])
class FlecheEvent(Event):
    TYPE_CHOICES = (('ACPF', 'ACPF'),
                    ('ACPT', 'ACPT'),
                    )
    class Meta:
        proxy = True
        verbose_name = "Fleche Team"
    objects = FlecheEventManager()

    def save(self, *args, **kwargs):
        super(Event, self).save(*args, **kwargs)

    def __unicode__(self):
        if self.name is None or self.name == "":
            name = "(no name)"
        else:
            name = self.name
        return unicode(self.ridegroup) + " " + str(self.date.year) + "(" + self.type + "): " + name + " -- " + str(self.distance) + "km"

class Eventresult(models.Model):
    eventid = models.OneToOneField(Event, db_column='eventID') # Field name made lowercase.
    status = models.CharField(max_length=135, blank=True)
    history = models.CharField(max_length=3072, blank=True)
    trouble = models.TextField(max_length=3072, blank=True)
    conditions = models.TextField(max_length=3072, blank=True)
    photos = models.CharField(verbose_name="Photos URL", max_length=1024, blank=True)
    reporturl = models.CharField(verbose_name="Report URL", max_length=1024, blank=True)
    eventresultid = models.AutoField(primary_key=True)
                              
    class Meta:
        db_table = u'eventresults'
        verbose_name = "Event Info"

    def __unicode__(self):
        return "[ Result for " + unicode(self.eventid) + "]"

    def to_dict(self):
        return { 'status' : self.status,
                 'conditions': self.conditions,
                 'photos' : self.photos,
                 'reporturl' : self.reporturl }

class Membershipdata(models.Model):
    memberid = models.ForeignKey(Member, db_column='memberID') # Field name made lowercase.
    year = models.DateField()
    membershipdataid = models.AutoField(primary_key=True)
    class Meta:
        db_table = u'membershipdata'

    def __unicode__(self):
        return "[ Membership data: " + str(self.memberid) + " in year " + (str(self.year.year) if self.year != None else "") + " ]"

class Rideresult(models.Model):
    BICYCLECLASS_CHOICES = (('Single', 'Single'),
                            ('Tandem', 'Tandem'),
                            ('Fixed', 'Fixed'),
                            ('Recumbent', 'Recumbent'),
                            ('Velomobile', 'Velomobile')
                            )

    eventid = models.ForeignKey(Event, db_column='eventID') # Field name made lowercase.
    memberid = models.ForeignKey(Member, db_column='memberID') # Field name made lowercase.
    acpnumber = models.IntegerField(null=True, db_column='acpNumber', blank=True, verbose_name='Homologation Number') # Field name made lowercase.
    acpregion = models.ForeignKey(Clubcode, verbose_name='Home Region')
    time = models.IntegerField(null=False, blank=True, default=-1)
    medal = models.BooleanField(null=False, blank=True, default=False)
    distanceoptional = models.IntegerField(null=True, db_column='distanceOptional', blank=True, verbose_name='22 Hour Distance') # Field name made lowercase.
    bicycleclass = models.CharField(max_length=135, db_column='bicycleClass', default='Single', choices=BICYCLECLASS_CHOICES, verbose_name='Bicycle Class') # Field name made lowercase.
    remarks = models.CharField(max_length=3072, blank=True)
    volunteerpreride = models.BooleanField(db_column='volunteerPreride', default=False, verbose_name='Volunteer Preride') # Field name made lowercase.
    official = models.BooleanField(default=False)
    distanceoptional2 = models.IntegerField(null=True, db_column='distanceOptional2', blank=True, verbose_name='24 Hour Distance') # Field name made lowercase.
    rideresultid = models.AutoField(primary_key=True)
    flechefinalcontrol = models.CharField(max_length=1024, blank=True, verbose_name='24h Contrtol')
    class Meta:
        db_table = u'rideresults'
        verbose_name = "Ride Result"

    def save(self, *args, **kwargs):
        if not hasattr(self, 'acpregion') or self.acpregion is None:
            self.acpregion = self.memberid.club
        super(Rideresult, self).save(*args, **kwargs)

    def __unicode__(self):
        return "(result: " + unicode(self.eventid) + "/" + unicode(self.memberid) + ")"

    def _formattime(self):
        if self.time > 0:
            formatted_time = minutesToTime(self.time/60/1000)
        else:
            formatted_time = self.time
        if self.official:
            return timeSplit(formatted_time)
        else:
            return timeSplit(formatted_time) + '*'
    formattime = property(_formattime)

    def _bikeclass(self):
        if self.bicycleclass is None or self.bicycleclass == "NULL":
            return "Single"
        else:
            return self.bicycleclass
    bikeclass = property(_bikeclass)

    def to_dict(self):
        return { 'id' : self.rideresultid,
                 'time': self.formattime,
                 'event_id': self.eventid.eventid,
                 'member_id': self.memberid.memberid,
                 'member': self.memberid.to_dict(),
                 'acpnumber': self.acpnumber,
                 'distanceoptional': self.distanceoptional,
                 'distanceoptional2': self.distanceoptional2,
                 'bicycleclass': self.bicycleclass,
                 'remarks': self.remarks,
                 'volunteerpreride': self.volunteerpreride,
                 'official': self.official,
                 'flechefinalcontrol': self.flechefinalcontrol
                 }

class Volunteer(models.Model):
    eventid = models.ForeignKey(Event, db_column='eventID') # Field name made lowercase.
    memberid = models.ForeignKey(Member, db_column='memberID') # Field name made lowercase.
    remarks = models.CharField(max_length=135, blank=True)
    organizer = models.BooleanField(default=False)
    volunteersid = models.AutoField(primary_key=True)
    class Meta:
        db_table = u'volunteers'

    def to_dict(self):
        return { 'member': self.memberid.to_dict(),
                 'organizer': self.organizer }
