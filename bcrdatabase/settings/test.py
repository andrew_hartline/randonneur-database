from bcrdatabase.settings.common import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'bcrandonneur_dat',
    }
}

STATIC_URL = '/static/'
