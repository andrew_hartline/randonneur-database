# Django settings for bcrdatabase project.
from os import path

project_path = path.join(path.dirname(__file__), "..")

ADMINS = [
    ('Ryan Golbeck', 'gowlin@gmail.com'),
    ('Etienne', 'rfc2324c418@gmail.com')
]

MANAGERS = ADMINS

MAIN_SITE = "http://www.randonneurs.bc.ca"
LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = '/'
LOGIN_REDIRECT_URL_FAILURE = '/'

# Test settings
BASE_PATH = project_path
TEST_DISCOVERY_ROOT = path.join(BASE_PATH, "tests")
TEST_RUNNER = "django.test.runner.DiscoverRunner"

MESSAGE_STORAGE = 'django.contrib.messages.storage.fallback.FallbackStorage'

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'America/Vancouver'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

USE_TZ = True

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

DATA_UPLOAD_MAX_NUMBER_FIELDS = 10240
# Additional locations of static files
STATICFILES_DIRS = (
    path.join(project_path, 'site_static'),
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [path.join(project_path, 'site_templates'),
                 path.join(project_path, 'admin_templates'), ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.request",
                "django.template.context_processors.tz",
                "django.contrib.messages.context_processors.messages",
                "bcrdatabase.bcrdata.context_processors.site_url",
                "bcrdatabase.bcrdata.context_processors.region_list",
            ]
        },
    },
]

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # 'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this uniquge, and don't share it with anybody.
SECRET_KEY = 'j(00$z$(nivr=moapz%1-2a(4#x0us798yie=2dbshnh*zw!&t'

MIDDLEWARE = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF = 'bcrdatabase.urls'

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'dal',
    'dal_select2',
    'django_markdown',
    'selectable',
    'favicon',

    'bcrdatabase.registration',
    'bcrdatabase.bcrdata',
    'bcrdatabase.reporting',
    'bcrdatabase.schedule',
    'bcrdatabase.management',
    'bcrdatabase.search',
    'bcrdatabase.api',
)

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'bcrdatabase': {
            'handlers': ['console'],
            'level': 'DEBUG',
        },
    }
}
