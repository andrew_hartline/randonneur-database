"""Django settings for development of bcrdata project.
"""
import os
from bcrdatabase.settings.common import *

DEBUG = True
ENABLE_TOOLBAR = True

DATABASES = {
    'default': {
        # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'ENGINE': 'django.db.backends.mysql',
        # Or path to database file if using sqlite3.
        'NAME': 'bcrandonneur_dat',
        'USER': 'root',                      # Not used with sqlite3.
        'PASSWORD': 'root',                  # Not used with sqlite3.
        # Set to empty string for localhost. Not used with sqlite3.
        'HOST': 'mysql',
        # Set to empty string for default. Not used with sqlite3.
        'PORT': '',
        'OPTIONS': {
            "init_command": "SET foreign_key_checks = 0;",
        },

    }
}

SITE_URL = 'https://' + os.getenv('SITE_URL', 'database.randonneurs.bc.ca')
if any(s in SITE_URL for s in ['localhost', '127.0.0.1', '[::1]']):
    SITE_URL = SITE_URL.replace('https', 'http')

STATIC_URL = '/static/'
MEDIA_URL = '/media/'
MEDIA_ROOT = path.join(project_path, 'media')
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

if DEBUG:
    ALLOWED_HOSTS = ['*']

if DEBUG and ENABLE_TOOLBAR:
    INTERNAL_IPS = ('127.0.0.1', '0.0.0.0', '172.18.0.1',)  # Docker host ip
    if not MIDDLEWARE:
        MIDDLEWARE = ()
    MIDDLEWARE += (
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    )

    INSTALLED_APPS += (
        'debug_toolbar',
    )

    DEBUG_TOOLBAR_PANELS = [
        'debug_toolbar.panels.versions.VersionsPanel',
        'debug_toolbar.panels.timer.TimerPanel',
        'debug_toolbar.panels.settings.SettingsPanel',
        'debug_toolbar.panels.headers.HeadersPanel',
        'debug_toolbar.panels.request.RequestPanel',
        'debug_toolbar.panels.sql.SQLPanel',
        'debug_toolbar.panels.staticfiles.StaticFilesPanel',
        'debug_toolbar.panels.templates.TemplatesPanel',
        'debug_toolbar.panels.cache.CachePanel',
        'debug_toolbar.panels.signals.SignalsPanel',
        'debug_toolbar.panels.logging.LoggingPanel',
        'debug_toolbar.panels.redirects.RedirectsPanel',
    ]

    DEBUG_TOOLBAR_CONFIG = {
        'INTERCEPT_REDIRECTS': False,
    }
