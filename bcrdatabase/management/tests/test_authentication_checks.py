from django.contrib.auth.models import User, AnonymousUser
from bcrdatabase.management.views import current_member_check, staff_check
from bcrdatabase.bcrdata.models import Member
from datetime import date, timedelta

from django.test import TestCase

class AuthenticationChecks(TestCase):
    """Tests for authentication check functions"""

    def test_current_member_check_anonymous(self):
        """Test the current member check"""
        user = AnonymousUser()
        
        self.assertFalse(current_member_check(user))

    def test_current_member_check_expired(self):
        """Test current member fails for expired user"""
        user = User.objects.create_user("user", "user@example.com", "password")
        member = Member.objects.create(email="user@example.com", expiredate=date.today() - timedelta(28))
        user.member = member
        user.save()

        self.assertFalse(current_member_check(user))

    def test_current_member_check_current(self):
        """Test current member fails for expired user"""
        user = User.objects.create_user("user", "user@example.com", "password")
        member = Member.objects.create(email="user@example.com", expiredate=date.today() + timedelta(28))
        user.member = member
        user.save()

        self.assertTrue(current_member_check(user))

    def test_staff_check_anonymous(self):
        """Anonymous is not staff"""
        user = AnonymousUser()

        self.assertFalse(staff_check(user))

    def test_staff_check_not_staff(self):
        """Test current user is not staff"""
        user = User.objects.create_user("user", "user@example.com", "password")
        member = Member.objects.create(email="user@example.com", expiredate=date.today() - timedelta(28))
        member.user = user
        member.save()
        user.save()

        self.assertFalse(staff_check(user))

    def test_staff_check(self):
        user = User.objects.create_superuser("user", "user@example.com", "password")

        self.assertTrue(staff_check(user))
