import unittest
from django.test import Client, TestCase
from django.contrib.auth.models import User
from bcrdatabase.bcrdata.models import Member, Clubcode, Rideresult
from bcrdatabase.bcrdata.tests.Mocks import MockEvents, MockMembers
from datetime import date

class RegionalDashboard(TestCase):
    """Tests for the Regional Dashboard"""

    def setUp(self):
        self.client = Client()

    def login(self):
        user = User.objects.create_superuser("super", "super@example.com", "super")
        member = MockMembers.create()
        member.email = "super@example.com"
        club = Clubcode.objects.create()
        member.admin_regions.add(club)
        member.save()
        member.user = user
        member.save()
        user.save()
        self.client.login(username="super", password="super")
        return user
        
    def test_regional_dashboard_redirect(self):
        """Regional dashboard redirects for anonymous user"""
        response = self.client.get('/management/regional_dashboard/', follow=True)
        self.assertRedirects(response, '/login/?next=/management/regional_dashboard/')

    def test_regional_dashboard_login(self):
        """Regional dashboard displays for super user"""
        user = self.login()
        response = self.client.get('/management/regional_dashboard/')
        self.assertEqual(response.status_code, 200)

    def test_edit_event_not_admin(self):
        event = MockEvents.create()
        user = self.login()
        response = self.client.post('/management/regional_dashboard/event/' + str(event.eventid) + '/')
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, "You are not an administrator of this event.")

    def test_edit_event_no_results(self):
        event = MockEvents.create()
        user = self.login()
        event.acpregion = user.member.admin_regions.all()[0]
        event.save()
        response = self.client.post('/management/regional_dashboard/event/' + str(event.eventid) + '/')
        self.assertEqual(response.status_code, 200)

    def test_edit_unhomologated_event(self):
        event = MockEvents.create()
        user = self.login()
        event.acpregion = user.member.admin_regions.all()[0]
        event.save()
        result = Rideresult.objects.create(memberid=user.member, eventid=event, time=10)
        response = self.client.post('/management/regional_dashboard/event/' + str(event.eventid) + '/')
        self.assertEqual(response.status_code, 200)

    def test_edit_homologated_event(self):
        event = MockEvents.create()
        user = self.login()
        event.acpregion = user.member.admin_regions.all()[0]
        event.save()
        result = Rideresult.objects.create(memberid=user.member, eventid=event, time=10, acpnumber=10)
        response = self.client.post('/management/regional_dashboard/event/' + str(event.eventid) + '/')
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, "This event has already been homologated.")
        

