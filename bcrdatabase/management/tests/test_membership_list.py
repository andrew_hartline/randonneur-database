import unittest
from django.test import Client, TestCase
from django.contrib.auth.models import User
from bcrdatabase.bcrdata.models import Member, Clubcode
from datetime import date, timedelta

class MembershipList(TestCase):
    """Tests for the Membership List"""

    def setUp(self):
        self.client = Client()
        
    def test_membership_list_redirect(self):
        """Membership list redirects for anonymous user"""
        response = self.client.get('/management/membership_list/', follow=True)
        self.assertRedirects(response, '/login/?next=/management/membership_list/')

    def test_membership_list_csv_redirect(self):
        """Membership list CSV redirects for anonymous user"""
        response = self.client.get('/management/membership_list_csv/', follow=True)
        self.assertRedirects(response, '/login/?next=/management/membership_list_csv/')

    def test_membership_list_current(self):
        """Membership list works for current member"""
        user = User.objects.create_superuser("super", "super@example.com", "super")
        member = Member.objects.create(email="super@example.com", expiredate=date.today() + timedelta(28))
        member.user = user
        member.save()

        self.client.login(username="super", password="super")
        response = self.client.get('/management/membership_list/')
        self.assertEqual(response.status_code, 200)

    def test_membership_list_csv_current(self):
        """Membership list CSV works for current member"""
        user = User.objects.create_superuser("super", "super@example.com", "super")
        member = Member.objects.create(email="super@example.com", expiredate=date.today() + timedelta(28))
        member.user = user
        member.save()

        self.client.login(username="super", password="super")
        response = self.client.get('/management/membership_list_csv/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], 'text/csv')

    def test_membership_list_expired(self):
        """Membership list redirects for expired member"""
        user = User.objects.create_superuser("super", "super@example.com", "super")
        member = Member.objects.create(email="super@example.com", expiredate=date.today() - timedelta(28))
        member.user = user
        member.save()

        self.client.login(username="super", password="super")
        response = self.client.get('/management/membership_list/')
        self.assertRedirects(response, '/login/?next=/management/membership_list/')

    def test_membership_list_csv_expired(self):
        """Membership list CSV redirects for expired member"""
        user = User.objects.create_superuser("super", "super@example.com", "super")
        member = Member.objects.create(email="super@example.com", expiredate=date.today() - timedelta(28))
        member.user = user
        member.save()

        self.client.login(username="super", password="super")
        response = self.client.get('/management/membership_list_csv/', follow=True)
        self.assertRedirects(response, '/login/?next=/management/membership_list_csv/')

