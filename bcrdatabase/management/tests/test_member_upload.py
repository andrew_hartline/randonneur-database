import unittest
from django.test import Client, TestCase
from django.contrib.auth.models import User
from bcrdatabase.bcrdata.models import Member
from datetime import date, timedelta

class MemberUpload(TestCase):
    """Tests for uploading the membership list"""

    def test_memberupload_redirect(self):
        """Membership upload form redirects for anonymous"""
        response = self.client.get('/management/upload_membership/', follow=True)

        self.assertRedirects(response, '/login/?next=/management/upload_membership/', status_code=302)

    def test_memberupload_denies_non_staff(self):
        """Membership upload rejects non-staff"""
        user = User.objects.create_user("user", "user@example.com", "password")
        member = Member.objects.create(email="user@example.com", expiredate=date.today() + timedelta(28))
        response = self.client.get('/management/upload_membership/', follow=True)
        self.client.login(username="user", password="password")
        
        self.assertRedirects(response, '/login/?next=/management/upload_membership/')

    def test_memberupload_shows_staff(self):
        """Test that staff gets the member upload form"""
        user = User.objects.create_superuser("user", "user@example.com", "password")
        member = Member.objects.create(email="user@example.com", expiredate=date.today() + timedelta(28))
        self.client.login(username="user", password="password")
        response = self.client.get('/management/upload_membership/')

        self.assertEqual(response.status_code, 200)
