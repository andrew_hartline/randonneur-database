from datetime import date
from django.shortcuts import render, HttpResponseRedirect, get_object_or_404
from django.template import Context, loader
from django.http import HttpResponse, Http404,HttpResponseRedirect
from django.template import RequestContext
from bcrdatabase.bcrdata.models import Member, currentMembers, Rideresult, filterForFinished, Event, filterInBC
from datetime import datetime
from django.contrib.auth.decorators import user_passes_test, login_required
from django.conf import settings
from bcrdatabase.management.upload_membership import MembershipUploadForm, handle_uploaded_membership_form

def staff_check(user):
    return user.is_staff

def current_member_check(user):
    return user.is_staff or (not user.is_anonymous() and hasattr(user, 'member') and user.member.is_current_member())

@user_passes_test(staff_check, login_url='/auth_error')
def unhomologated_events(request):
    eventids = filterInBC(filterForFinished(Rideresult.objects.filter(official=1,acpnumber__isnull=True,eventid__date__gte='2005-01-01').exclude(eventid__type='BCP')).order_by('-eventid__date').values('eventid').distinct())
    events = map(lambda id: Event.objects.get(pk=id['eventid']), eventids)

    return render(request, 'management/unhomologated-events.html',
                  { 'events' : events,
                  })



@user_passes_test(staff_check, login_url='/auth_error')
def unofficial_results(request):
    return render(request, 'management/unofficial-results.html',
                  { 'results' : Rideresult.objects.filter(official=0).exclude(eventid__type='BCP'), })

@user_passes_test(current_member_check, login_url='/auth_error')
def membership_list(request):
    return render(request, 'management/memberlist.html',
                              { 'members' : currentMembers(),
                                'user': request.user,
                                })

@user_passes_test(staff_check, login_url='/auth_error')
def upload_membership(request):
    if request.method == 'POST':
        form = MembershipUploadForm(request.POST, request.FILES)
        if form.is_valid():
            results = handle_uploaded_membership_form(request.FILES['file'])
            return render(request, 'management/upload_membership_results.html',
                          {'results' : results })
    else:
        form = MembershipUploadForm()
    return render(request, 'management/upload_membership.html',
                              {'form' : form})

@user_passes_test(current_member_check, login_url='/auth_error')
def membership_list_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=BCRMemberList{0}.csv'.format(datetime.today().year)
    
    template_file = 'management/memberlist.csv' if request.user.is_staff else 'management/memberlist_slim.csv'
    template = loader.get_template(template_file)
    response.write(template.render({ 'members': currentMembers() }))
    return response
