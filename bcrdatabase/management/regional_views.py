from datetime import date

from bcrdatabase.bcrdata.models import Event
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.forms import ModelForm
from django.http import HttpResponseForbidden
from django.shortcuts import HttpResponseRedirect, get_object_or_404, render
from django.utils import timezone

"""views specific to regional administrators"""


class EventEditForm(ModelForm):
    """Form for editing the data of an event"""
    class Meta:
        model = Event
        fields = ['name', 'start', 'controlcard', 'routesheet', 'gpx']


@login_required
def regional_dashboard_event(request, eventid):
    """Dashboard for a particular event"""

    event = get_object_or_404(Event, pk=eventid)
    user_administers_event_region = request.user.member.admin_regions \
        .filter(idclubcodes=event.acpregion.idclubcodes) \
        .exists()
    if not user_administers_event_region:
        return HttpResponseForbidden("You are not an administrator of this event.")

    if request.method == 'POST':
        if event.resultsHomologated():
            return HttpResponseForbidden("This event has already been homologated.")

        event_form = EventEditForm(request.POST, request.FILES, instance=event)
        if event_form.is_valid():
            event_form.save()
            messages.add_message(request,
                                 messages.INFO,
                                 'Event ' + str(event) + " updated at " +
                                 str(timezone.now().strftime('%Y-%m-%d %H:%M')))

            if '_save' in request.POST and 'next' in request.POST:
                return HttpResponseRedirect(request.POST.get('next'))
            elif '_save' in request.POST:
                redirect = reverse('regional_dashboard')
                return HttpResponseRedirect(redirect)
            else:
                event_form = EventEditForm(
                    instance=get_object_or_404(Event, pk=eventid))
    else:
        event_form = EventEditForm(instance=event)

    return render(request, 'management/regional/edit-event.html',
                  {'event': event,
                   'volunteers': event.volunteer_set.all(),
                   'eventForm': event_form})


@login_required
def regional_dashboard(request):
    """The dashboard for a regional administrator"""
    if not request.user.member.is_regional_admin():
        return HttpResponseForbidden("You are not a regional administrator.")

    events_in_member_region = Event.objects \
        .filter(acpregion__administrators__memberid=request.user.member.memberid)

    regions_administered = request.user.member.admin_regions.all()
    event_list = Event.filterACPBrevetsInYear(
        timezone.now().year, events_in_member_region)

    return render(request, 'management/regional/dashboard.html',
                  {'event_list': event_list,
                   'regions_administered': regions_administered})
