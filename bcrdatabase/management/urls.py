from django.conf.urls import include, url
from bcrdatabase.management.views import *
from bcrdatabase.management.regional_views import *

urlpatterns = [
    url(r'^membership_list/$', membership_list, name='membership_list'),
    url(r'^membership_list_csv/$', membership_list_csv, name='membership_list_csv'),

    url(r'^upload_membership/$', upload_membership, name='upload_membership'),

    url(r'^reports/unofficial/$', unofficial_results, name='unofficial_results'),
    url(r'^reports/unhomologated/$', unhomologated_events, name='unhomologated_events'),

    # Regional Brevet Administration
    url(r'^regional_dashboard/$', regional_dashboard, name="regional_dashboard"),
    url(r'^regional_dashboard/event/(?P<eventid>\d+)/$', regional_dashboard_event, name='regional_dashboard_event'),
]    
