from django import forms
import csv
import re
from datetime import date, datetime
from tempfile import NamedTemporaryFile
from bcrdatabase.bcrdata.models import Member

import logging

logger = logging.getLogger(__name__)

class MultipleMembers(Exception):
    def __init__(self, member_info):
        self.member_info = member_info

    def __str__(self):
        return "Found multiple members for: " + str(self.member_info)

class MemberResult:
    def __init__(self, member, result_message):
        self.member = member
        self.result_message = result_message

    def message(self):
        return self.result_message + ": " + str(self.member)

class MemberInfo:
    def parseValue(self, index):
        value = self.member_info[self.headers.index(index)]
        if len(value) > 0 and value[0] == '=':
            return value[2:len(value)-1]
        else:
            return value
    
    def __init__(self, headers, member_info):
        self.headers = headers
        self.member_info = member_info
        
        self.registration_date = datetime.strptime(self.parseValue('Reg Checkout Date'), "%Y/%m/%d %H:%M:%S").date()
        self.ccn_id = self.parseValue('Identity ID')
        self.member_number = self.parseValue('BC Randonneurs Member Number')
        self.lastname = self.parseValue('Last Name')
        self.firstname = self.parseValue('First Name')
        self.email = self.parseValue('Email')
        self.address = self.parseValue('Registrant Street 1')
        billing_two = self.parseValue('Registrant Street 2')
        if (billing_two != ''):
            self.address = self.address + " / " + billing_two
        self.city = self.parseValue('Registrant City')
        self.province = self.parseValue('Registrant Province')
        self.postal = self.parseValue('Registrant Postal Code')
        self.country = self.parseValue('Registrant Country')
        self.phone = self.parseValue('Registrant Telephone')
        gender = self.parseValue('Gender (used for ridership reports only)')
        birthday = self.parseValue('DOB')
        if birthday != "":
            self.birthday = datetime.strptime(birthday, "%Y/%m/%d").date()
        else:
            self.birthday = None
        self.emergencyname = self.parseValue('Emergency Contact Name') 
        self.emergencyphone = self.parseValue('Emergency Contact Phone Number') 
        info_release = self.parseValue('The club will compile and distribute (to members only) a current Membership List that will include name, address, telephone number and e-mail address.') == 'include my contact information on this list'
        bccc = self.parseValue('Addons Purchased During Reg') == 'Join the BC Cycling Coalition '

        if gender == 'Male':
            self.gender = 'M'
        elif gender == 'Female':
            self.gender = 'F'
        else:
            self.gender = 'X'
            
        if info_release:
            self.info_release=True
        else:
            self.info_release=False
        if bccc:
            self.bccc=True
        else:
            self.bccc=False

        waiver = self.parseValue('Waiver')
        if waiver:
            self.waiver = True
        else:
            self.waiver = False

    def __str__(self):
        return "[lastname= " + self.lastname + "; firstname=" + self.firstname + "; ccn_id=" + self.ccn_id + "; member_id=" + self.member_number + "]"
    

class MembershipUploadForm(forms.Form):
    file = forms.FileField()

def find_member_by_ccn(member_info):
    members = Member.objects.filter(ccnid=member_info.ccn_id)
    if len(members) == 1:
        return members[0]
    return None

def find_member_by_member_number(member_info):
    try :
        members = Member.objects.filter(memberid=member_info.member_number,lastname__iexact=member_info.lastname)
        members_without_lastname = Member.objects.filter(memberid=member_info.member_number)
        if len(members) != len(members_without_lastname):
            raise MultipleMembers(member_info)
            if len(members) == 1:
                return members[0]
    except Exception as e:
        logger.warn("Error looking up member by number: " + str(e))
    return None

def find_member_by_name(member_info):
    members = Member.objects.filter(lastname__iexact=member_info.lastname,firstname__icontains=member_info.firstname)
    count = len(members)
    if count == 1:
        return members[0]
    elif count > 1:
        raise MultipleMembers(member_info)
    return None

def update_member(member, member_info):
    member.ccnid = member_info.ccn_id
    member.emergencyname = member_info.emergencyname
    member.emergencynumber = member_info.emergencyphone
    member.info_release = member_info.info_release
    member.bcc = member_info.bccc
    member.expiredate = registration_date(member_info.registration_date)
    member.waiver = member_info.waiver
    member.save()

    return MemberResult(member, "Updated membership info")

def create_member(member_info):
    member = Member()
    member.ccnid = member_info.ccn_id
    member.lastname = member_info.lastname
    member.firstname = member_info.firstname
    member.email = member_info.email
    member.gender = member_info.gender
    member.address = member_info.address
    member.province = member_info.province
    member.postalcode = member_info.postal
    member.country  = member_info.country
    member.evephone = member_info.phone
    member.gender = member_info.gender
    member.emergencyname = member_info.emergencyname
    member.emergencynumber = member_info.emergencyphone
    member.info_release = member_info.info_release
    member.bcc = member_info.bccc
    member.expiredate = registration_date(member_info.registration_date)
    member.waiver = member_info.waiver
    member.birthday = member_info.birthday
    member.save()

    return MemberResult(member, "Created member (" + str(member.memberid) + ")")

def find_member(member_info):
    member = None
    if member_info.ccn_id != "":
        member = find_member_by_ccn(member_info)

    if member == None and member_info.member_number != "":
        member = find_member_by_member_number(member_info)

    if member == None:
        member = find_member_by_name(member_info)

    return member

def registration_date(reg_date):
    if reg_date.month >= 8:
        return date(reg_date.year + 2, 1, 1)
    return date(reg_date.year + 1, 1, 1)

def process_result(member_info):
    member = find_member(member_info)

    if member == None:
        result = create_member(member_info)
    elif member.expiredate >= registration_date(member_info.registration_date):
        result = MemberResult(member, "Membership already up to date")
    else:
        result = update_member(member, member_info)

    return result

def csv_reader(csv):
    # Skip first two lines of CCN
    csv.next()
    csv.next()
    headers = csv.next()
    print headers
    results = []
    while True:
        try:
            member_info = csv.next()
            result = process_result(MemberInfo(headers, member_info))
            if result != None:
                results.append(result)
        except MultipleMembers as m:
            results.append(MemberResult(None, m.__str__()))
        except StopIteration, e:
            return results

def get_tmp_filename(file):
    return "/tmp/" + file.name

def save_temp_file(f):
    with open(get_tmp_filename(f), 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
        return destination.name

def handle_uploaded_membership_form(file):
    tmp_file =  save_temp_file(file)
    with open(tmp_file, 'r') as input:
        return csv_reader(csv.reader(input))

