from django.shortcuts import render
from django.template import RequestContext
from django import forms
from django.db.models import Q
from bcrdatabase.bcrdata.models import Event, Member, Route
from datetime import datetime, date

class MemberSearchForm(forms.Form):
    name = forms.CharField(max_length=40)

class RouteSearchForm(forms.Form):
    TYPE_CHOICE=(("Any", "Any"),
                 ("ACPB", "Brevet"),
                 ("BCP", "Permanent"),
                 )

    controls = forms.CharField(max_length=60, required=False)
    above_kms = forms.IntegerField(required=False,label='Above km')
    below_kms = forms.IntegerField(required=False,label='Below km')
    type = forms.ChoiceField(required=False,choices=TYPE_CHOICE)

class EventSearchForm(forms.Form):
    DISTANCE_CHOICE=(("Any", "Any"),
                     ("200", "200"),
                     ("300", "300"),
                     ("400", "400"),
                     ("600", "600"),
                     ("1000", "1000"),
                     ("1200", "1200"),
                     ("2000", "2000"),
                     )
    TYPE_CHOICE=(("ACPB", "ACP Brevet"),
                 ("RM", "RM Brevet"),
                 ("BCP", "BC Permanent"),
                 ("ACPF", "ACP Fleche"),
                 ("ACPT", "ACP Trace"),
                 ("Any", "Any"),
                 )

    name = forms.CharField(max_length=40,required=False,label='Event or Route name')
    distance = forms.ChoiceField(required=False,choices=DISTANCE_CHOICE)
    event_type = forms.ChoiceField(required=False,choices=TYPE_CHOICE)
    year = forms.IntegerField(required=False)
    #include_out_of_province_events = forms.BooleanField(required=False)



def eventSearch(request):
    if request.method == 'GET':
        form = EventSearchForm(request.GET)
        if form.is_valid():
            search_name = form.cleaned_data['name']
            search_distance = form.cleaned_data['distance']
            search_year = form.cleaned_data['year']
            search_type = form.cleaned_data['event_type']
            #search_oop = form.cleaned_data['include_out_of_province_events']

            try:
                date(search_year, 1, 1)
            except ValueError, e:
                search_year = ""
            except TypeError, e:
                search_year = ""

            search = None
            if search_name != None and search_name != "":
                search = Q(name__icontains=search_name)
            if search_distance != None and search_distance != "Any":
                if search == None:
                    search = Q(distance=search_distance)
                else:
                    search = search & Q(distance=search_distance)
            if search_year != None and search_year != "":
                if search == None:
                    search = Q(date__year=search_year)
                else:
                    search = search & Q(date__year=search_year)
            if search_type != None and search_type != "Any":
                if search == None:
                    search = Q(type=search_type)
                else:
                    search = search & Q(type=search_type)

            if search == None:
                events = Event.objects.all().order_by('date')
            else:
                events = Event.objects.filter(search).order_by('date')

            return render(request, 'search/event-search.html',
                                      { 'event_results' : events,
                                        'event': {'type': ''},
                                        })
        else:
            print "Form not valid?"
    return searchIndex(request)

def routeSearch(request):
    if request.method == 'GET':
        form = RouteSearchForm(request.GET)
        if form.is_valid():
            search = form.cleaned_data['controls']
            above = form.cleaned_data['above_kms']
            below = form.cleaned_data['below_kms']
            rtype = form.cleaned_data['type']
            
            query = Q()
            if search != None:
                query = query & Q(controls__icontains=search)
            if above != None:
                query = query & Q(distance__gte=above)
            if below != None:
                query = query & Q(distance__lte=below)
            if rtype == "BCP":
                query = query & Q(permanentid__isnull=False)
            if rtype == 'ACPB':
                query = query & Q(brevet=True)

            routes = Route.objects.filter(query).order_by('acpregion__name')

            return render(request, 'search/route-search.html',
                                      { 'route_results': routes,
                                        })
    return searchIndex(request)

def memberSearch(request):
    if request.method == 'GET':
        form = MemberSearchForm(request.GET)
        if form.is_valid():
            search = form.cleaned_data['name']
            words = search.split()
            search_query = None
            for word in words:
                additional_query = (Q(lastname__icontains=word) | Q(firstname__icontains=word))
                if search_query == None:
                    search_query = additional_query
                else:
                    search_query = search_query & additional_query

            search_results = Member.objects.filter(search_query).order_by('lastname', 'firstname')

            return render(request, 'search/member-search.html',
                                      { 'event': {'type':''},
                                        'member_results': search_results,
                                        'search_string': search
                    })
    return searchIndex(request)

def index(request):
    memberSearchForm = MemberSearchForm()
    eventSearchForm = EventSearchForm()
    routeSearchForm = RouteSearchForm()

    return render(request, 'search/index.html',
                              { 'memberform': memberSearchForm,
                                'eventform': eventSearchForm,
                                'routeform': routeSearchForm,
                                'event': { 'type' : "" },
                                })
