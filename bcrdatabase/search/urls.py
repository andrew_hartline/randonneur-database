from django.conf.urls import include, url
from bcrdatabase.search.views import *
               
urlpatterns = [url(r'^$', index),
               url(r'^member-search$', memberSearch),
               url(r'^event-search$', eventSearch),
               url(r'^route-search$', routeSearch),
]
