from django import forms
from django.contrib.auth import (
    authenticate, get_user_model, password_validation,
)
from django.contrib.auth.models import User
from django.contrib.auth.forms import UsernameField
from django.utils.translation import ugettext, ugettext_lazy as _

from bcrdatabase.bcrdata.models import Member

class SignUpForm(forms.ModelForm):
    email = forms.EmailField(max_length=254, help_text='Use the email you used for CCN registration or put on your membership form.')
    member_number = forms.IntegerField(help_text='Available by searching your member name')

    def clean(self):
        cleaned_data = super(SignUpForm, self).clean()
        try:
            member = Member.objects.get(pk=cleaned_data.get('member_number'))
            if member.user != None:
                raise forms.ValidationError('Member already has an account configured')
            if member.email != cleaned_data.get('email'):
                raise forms.ValidationError('Email address does not match member email address')
        except Member.DoesNotExist:
            raise forms.ValidationError('Member number does not exist')

    class Meta:
        model = User
        fields = ('username', 'email', 'member_number')
        field_classes = {'username': UsernameField}

    
class ProfileForm(forms.ModelForm):
    password1 = forms.CharField(
        label=_("Change Password"),
        strip=False,
        required=False,
        widget=forms.PasswordInput,
        help_text=password_validation.password_validators_help_text_html(),
    )
    password2 = forms.CharField(
        label=_("Changed Password confirmation"),
        strip=False,
        required=False,
        widget=forms.PasswordInput,
        help_text=_("Enter the same password as before, for verification."),
    )

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        self.instance.username = self.cleaned_data.get('username')
        password_validation.validate_password(self.cleaned_data.get('password2'), self.instance)
        return password2
    
    def save(self, commit=True):
        member = super(ProfileForm, self).save(commit)
        if self.cleaned_data["password1"] != "":
            member.user.set_password(self.cleaned_data["password1"])
            member.user.save()
        return member

    class Meta:
        model = Member
        fields = ('lastname', 'firstname', 'email', 'dayphone', 'evephone', 'emergencyname', 'emergencynumber', 'info_release')
