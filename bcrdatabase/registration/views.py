from django.http import Http404
from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from bcrdatabase.registration.forms import SignUpForm, ProfileForm
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.views.decorators.cache import never_cache

import logging
import random
import string

from bcrdatabase.bcrdata.models import Member

logger = logging.getLogger(__name__)

@login_required
@never_cache
def profile(request):
    member = request.user.member
    if request.method == 'POST':
        form = ProfileForm(request.POST, instance=member)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.INFO, "Saved")
    else:
        form = ProfileForm(instance=member)    

    return render(request, 'registration/profile.html',
                  { 'form': form,
                    'message': 'Success',
                  })

@never_cache
def signup(request):
    if request.user.is_authenticated():
        return redirect('home')
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            member = Member.objects.get(pk=form.cleaned_data.get('member_number'))
            password = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))
            user = form.save()
            user.set_password(password)
            member.user = user
            member.save()
            user.save()

            subject = 'BC Randonneurs Sign up Request'
            message = render_to_string('registration/account_activation_email.html', {
                'user': user,
                'password': password,
            })
            email_count = user.email_user(subject, message)
            logger.info('Sending signup email to ', user)
            
            return render(request, 'registration/signup-success.html')
    else:
        form = SignUpForm()
    return render(request, 'registration/signup.html', {'form': form})
