from django.dispatch import receiver
from django.db.models.signals import post_save
from django.contrib.auth.models import User

from bcrdatabase.bcrdata.models import Member

@receiver(post_save, sender=Member)
def save_member_data(sender, instance, **kwargs):
    if instance.user != None: 
        instance.user.first_name = instance.firstname
        instance.user.last_name = instance.lastname
        instance.user.email = instance.email
        instance.user.save()

@receiver(post_save, sender=User)
def save_user_data(sender, instance, **kwargs):
    try:
        if instance.member != None and instance.member.email != instance.email:
            instance.member.email = instance.email
            instance.member.save()
    except Member.DoesNotExist:
        pass
