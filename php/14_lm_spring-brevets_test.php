<HTML>
<HEAD>
  <META NAME="GENERATOR" CONTENT="Adobe PageMill 3.0 Win">
  <TITLE>Lower Mainland Spring Brevets</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"></HEAD>
<BODY BGCOLOR="#ffffff" text="#3151A1">
<CENTER><TABLE WIDTH="100%" BORDER="0" CELLPADDING="5" CELLSPACING="0" bordercolor="#3151A1" bgcolor="#3151A1">
  <TR>
    <TD WIDTH="33%" BGCOLOR="#FFFFFF">
      <FONT FACE="Arial"><A HREF="14_times.html">2014 Results</A></FONT></TD> 
    <TD WIDTH="34%" VALIGN="TOP" BGCOLOR="#FFFFFF">
    <P><CENTER>
      <FONT FACE="Arial">BC Randonneurs
    Cycling Club</FONT>
    </CENTER></TD>
    <TD WIDTH="33%" BGCOLOR="#FFFFFF" NOWRAP></TD>
  </TR>
</TABLE></CENTER></P>

<P><CENTER>&nbsp;</CENTER></P>

<P><CENTER><B><FONT SIZE="+2" FACE="Arial">2014
Lower Mainland Spring Brevet Series</FONT></B>
</CENTER></P>

<P>
<CENTER>
  <p><FONT FACE="Arial">Lower Mainland Brevet
    Coordinators:<BR>
  </FONT><font size="+1" face="Arial">Gary Baker &amp; Chris Cullum</font></p>
  <p>&nbsp;</p>

  <?php 
     $event_one = file_get_contents('http://localhost:8000/api/events/1989');
     $json_one = json_decode($event_one);

     $event_two = file_get_contents('http://localhost:8000/api/events/1990');
     $json_two = json_decode($event_two);

     $event_three = file_get_contents('http://localhost:8000/api/events/1991');
     $json_three = json_decode($event_three);

     $event_four = file_get_contents('http://localhost:8000/api/events/1993');
     $json_four = json_decode($event_four);

     $events = array($json_one, $json_two, $json_three, $json_four);
     ?>


  <table width="780" border="1" cellpadding="2" cellspacing="2" bordercolor="#3151A1">
    <tr>
      <?php foreach ($events as $event) {
	    ?>
      <td WIDTH="24%" BGCOLOR="#ffffff" NOWRAP VALIGN="TOP">
	<p>
	  <center>
	    <font size="+2" face="Arial"><?php echo $event->{'event'}->{'distance'}; ?>  km</font><br/>	    
	    <font face="Arial"><i><strong>&quot;<?php echo $event->{'event'}->{'name'}; ?>&quot;</strong></i></font><br/>
	    <font size="-1" face="Arial">
	      <?php echo $event->{'event'}->{'date'}; ?><br/>
	    </font>
	    <font size="-1" face="Arial">
	      Ride Organizer(s): <br/>
	    <?php foreach ($event->{'event'}->{'volunteers'} as $volunteer) { 
	    		if ($volunteer->{'organizer'}) {
	                  echo $volunteer->{'member'}->{'firstname'} . " " . $volunteer->{'member'}->{'lastname'};
         	          echo "<br/>";
                        } 
                  }  ?>
	    </font>
	  </center>
	</p>
	<p>
	  <center>
	    <font size="-1" face="Arial">
	      <?php 
		 $event_url = $event->{'event'}->{'url'}; /*$event->{'event'}->{'route'}->{'url'};*/
		 if ($event_url != "") { echo "<a href='" . $event_url . "'>Route</a>"; } else { echo "Route"; }
	         echo "&nbsp; &nbsp; ";
	         $gpx = $event->{'event'}->{'gpx'};
	         if ($gpx != "") { echo "<a href='" . $gpx . "'>GPX</a>"; } else { echo "GPX"; }
	         echo "&nbsp; &nbsp; ";
	         $routesheet = $event->{'event'}->{'routesheet'};
	         if ($routesheet != "") { echo "<a href='" . $routesheet . "'>RouteSheet</a>"; } else { echo "RouteSheet"; }
	      ?>
	  </center>
	</p>
	<p>
	  <center>
	    <font size="-1" face="Arial">
	      <?php echo "<a href='http://localhost:8000/event/" . $event->{'event'}->{'id'} . "'>View in database</a>" ?>
	    </font>
	  </center>
	</p>
	<p>
	  <center>
	    <font size="-1" face="Arial">
	      <?php 
		 $eventresult = $event->{'event'}->{'eventresult'};
	         if ($eventresult->{'photos'} != "") { echo "<a href='" . $eventresult->{'photos'} . "'>Photos</a>"; }
	         if ($eventresult->{'reporturl'} != "") { echo "<a href='" . $eventresult->{'reporturl'} . "'>Report</a>"; }
	      ?>
	    </font>
	  </center>
	</p>
      </td>
      <?php } ?>
    </tr>

    <tr>
      <?php foreach ($events as $event) {
	    ?>
      <td width="24%" bgcolor="#ffffff" nowrap valign="top">
	<p>
	  <font size="-1" face="Arial">Ride-Day Volunteers: <br/>
	    <?php foreach ($event->{'event'}->{'volunteers'} as $volunteer) { 
	    		if (!$volunteer->{'organizer'}) {
	                  echo $volunteer->{'member'}->{'firstname'} . " " . $volunteer->{'member'}->{'lastname'};
         	          echo "<br/>";
                        } 
                  }  ?>
	  </font>
	</p>
      </td>
      <?php } ?>

    <tr>
      <?php foreach ($events as $event) {
	    $eventresult = $event->{'event'}->{'eventresult'};
	    ?>
      <td width="24%" bgcolor="#ffffff" valign="top">
	<p>
	  <font size="-1" face="Arial">
	    Conditions: <br/>
	    <?php echo $eventresult->{'conditions'}; ?>
	  </font>
	</p>
      </td>
      <?php } ?>

    <tr>

      <?php foreach ($events as $event) {
	    ?>
      <?php $finisher_count = 0; ?>
      <td width="24%" bgcolor="#ffffff" nowrap valign="top">
	<p>
	  <table width="100%" bgcolor="#ffffff" nowrap>
	      <?php 
		 foreach ($event->{'event'}->{'results'} as $result) {
	            echo "<tr>";
	            echo "<td align='left'><font size='-1' face='Arial'>" . $result->{'member'}->{'firstname'} . " " . $result->{'member'}->{'lastname'} . "</font></td>";
	            echo "<td align='right'><font size='-1' face='Arial'>" . $result->{'time'} . "</font></td>";
		    echo "</tr>";

	            if ($result->{'time'} > 0) {
	      	       $finisher_count++;
	            }
	         }
	      ?>
	  </table>
	</p>
	<p>
	  <font size="-1" face="Arial"><center><b><?php echo $finisher_count; ?> Finishers</b></center></font>
	</p>
      </td>
      <?php } ?>
    </tr>
  </table>

  <p><FONT SIZE="-1" FACE="Arial">Times
measured in hours and minutes - hh:mm<BR>
<B>VP</B> = Volunteer Pre-ride<BR>
<B>TA</B> = Tandem<BR>
<b>RE</b>= Recumbent<BR>
<B>FX</B> = Fixed Gear<BR>
<B>SG</B> = Single Gear</FONT></p>
</CENTER></P>

<P>&nbsp;</P>

<P><font color="#FFFFFF">_
</font>
</BODY>
</HTML>
